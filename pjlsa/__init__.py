# We would love to use a Local version specifier as per [PEP440][],
# since this would be semantically correct. However, our version string
# ends up as an entry point value in `setup.py` and their grammar
# prohibits use of the plus sign. Thus, a post release will have to do.
#
# [PEP440]: https://peps.python.org/pep-0440/#local-version-identifiers
__version__ = "0.2.17.post1"

__cmmnbuild_deps__ = [
    {"product": "log4j", "groupId": "log4j"},
    {"product": "lsa-client", "groupId": "cern.lsa"}
]

# Fix for usage outside of CERN. As in the [README][] file, outside of
# CERN, `__gradle_deps__` must be specified. Without this,
# `cmmnbuild_dep_manager` fails to resolve this package, making the
# entire deployment permanently unresolvable. This means that 1. Gradle
# has to be run on every `resolve()` call and 2. instantiating
# `LSAClientGSI` twice raises an exception.
#
# The concrete gradle dependencies don't matter terribly because at GSI,
# `pjlsa` is only ever used indirectly via one of `pjlsa_gsipro`,
# `pjlsa_gsiint` and `pjlsa_gsidev`. Those packages specify the true
# dependencies.
#
# [README]: https://gitlab.cern.ch/scripting-tools/cmmnbuild-dep-manager#usage-outside-of-cern
__gradle_deps__ = [
    {"groupId": "log4j",
     "product": "log4j",
     "version": "1.2.17",
     "repository": 'maven { url "https://artifacts.acc.gsi.de/repository/default/" }'}
]

__stubgen_packages__ = [
    "java",
    "com.google.common.collect",
    "com.google.common.base",
    "cern.lsa",
    "cern.accsoft.commons",
    "cern.rbac.common",
    "cern.japc.core",
    "cern.japc.value"
]

from ._pjlsa import BaseLSAClient, LSAClient, LSAClientGSI

__all__ = ["BaseLSAClient", "LSAClient", "LSAClientGSI"]
