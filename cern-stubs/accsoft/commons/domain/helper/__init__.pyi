import java.util
import typing



class AccsoftDomainHelper:
    """
    public class AccsoftDomainHelper extends java.lang.Object
    
        Helper methods intended to be used internally.
    """
    _immutableListOf__E = typing.TypeVar('_immutableListOf__E')  # <E>
    @staticmethod
    def immutableListOf(*e: _immutableListOf__E) -> java.util.List[_immutableListOf__E]:
        """
        
            Parameters:
                elements (E...): elements of the list
        
            Returns:
                immutable list of the provided elements
        
        
        """
        ...
    _immutableMapByNameOf__E = typing.TypeVar('_immutableMapByNameOf__E')  # <E>
    @staticmethod
    def immutableMapByNameOf(class_: typing.Type[_immutableMapByNameOf__E]) -> java.util.Map[str, _immutableMapByNameOf__E]:
        """
        
            Parameters:
                enumClass (java.lang.Class<? extends E> enumClass): enumeration class
        
            Returns:
                immutable map of all the enumeration values pointed to by their names
        
        
        """
        ...
    _immutableSetOf_0__E = typing.TypeVar('_immutableSetOf_0__E')  # <E>
    _immutableSetOf_1__E = typing.TypeVar('_immutableSetOf_1__E')  # <E>
    @typing.overload
    @staticmethod
    def immutableSetOf(class_: typing.Type[_immutableSetOf_0__E]) -> java.util.Set[_immutableSetOf_0__E]:
        """
        
            Parameters:
                enumClass (java.lang.Class<? extends E> enumClass): enumeration class
        
            Returns:
                immutable set of all the enumeration values
        
        @SafeVarargs public static <E> java.util.Set<E> immutableSetOf (E... elements)
        
        
            Parameters:
                elements (E...): elements of the set
        
            Returns:
                immutable set of the provided elements
        
        
        """
        ...
    @typing.overload
    @staticmethod
    def immutableSetOf(*e: _immutableSetOf_1__E) -> java.util.Set[_immutableSetOf_1__E]: ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.domain.helper")``.

    AccsoftDomainHelper: typing.Type[AccsoftDomainHelper]
