import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.beamdestinations
import cern.accsoft.commons.domain.beams
import cern.accsoft.commons.domain.particletransfers
import cern.accsoft.commons.domain.zones
import cern.accsoft.commons.util
import java.util
import typing



class AccsoftDomainUtil:
    """
    public class AccsoftDomainUtil extends java.lang.Object
    
        Utility methods to be used by clients.
    """
    _findAcceleratorZone__T = typing.TypeVar('_findAcceleratorZone__T', bound=cern.accsoft.commons.domain.Accelerator)  # <T>
    @staticmethod
    def findAcceleratorZone(class_: typing.Type[_findAcceleratorZone__T], string: str) -> cern.accsoft.commons.domain.zones.AcceleratorZone:
        """
            Finds accelerator zone.
        
            Parameters:
                acceleratorEnumClass (java.lang.Class<T> acceleratorEnumClass): accelerator enumeration class
                acceleratorZoneName (java.lang.String): accelerator zone name
        
            Returns:
                :class:`~cern.accsoft.commons.domain.zones.AcceleratorZone`
        
            Raises:
                java.lang.IllegalArgumentException: if accelerator or zone doesn't exist
        
        
        """
        ...
    @staticmethod
    def findBeam(accelerator: cern.accsoft.commons.domain.Accelerator, string: str) -> cern.accsoft.commons.domain.beams.Beam:
        """
            Finds accelerator beam.
        
            Parameters:
                accelerator (:class:`~cern.accsoft.commons.domain.Accelerator`): accelerator
                beamName (java.lang.String): beam name
        
            Returns:
                :class:`~cern.accsoft.commons.domain.beams.Beam`
        
            Raises:
                java.lang.IllegalArgumentException: if beam doesn't exist
        
        
        """
        ...
    @staticmethod
    def findBeamDestinationEndPoint(beamDestination: cern.accsoft.commons.domain.beamdestinations.BeamDestination, string: str) -> cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint:
        """
            Finds beam destination end point by name.
        
            Parameters:
                beamDestination (:class:`~cern.accsoft.commons.domain.beamdestinations.BeamDestination`):         endPointName (java.lang.String): 
            Returns:
                :class:`~cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint`
        
            Raises:
                java.lang.IllegalArgumentException: if end point doesn't exist
        
        
        """
        ...
    _findNamedEnumValue__T = typing.TypeVar('_findNamedEnumValue__T', bound=cern.accsoft.commons.util.Named)  # <T>
    @staticmethod
    def findNamedEnumValue(class_: typing.Type[_findNamedEnumValue__T], string: str) -> _findNamedEnumValue__T:
        """
            Finds beam mode by name.
        
            Parameters:
                enumClass (java.lang.Class<T> enumClass): beam mode enumeration class
                enumValueName (java.lang.String): beam mode name
        
            Returns:
                beam mode
        
            Raises:
                java.lang.IllegalArgumentException: if beam mode doesn't exist
        
        
        """
        ...
    _findParticleTransfer__T = typing.TypeVar('_findParticleTransfer__T', bound=cern.accsoft.commons.domain.Accelerator)  # <T>
    @staticmethod
    def findParticleTransfer(class_: typing.Type[_findParticleTransfer__T], string: str) -> cern.accsoft.commons.domain.particletransfers.ParticleTransfer:
        """
            Finds particle transfer.
        
            Parameters:
                acceleratorEnumClass (java.lang.Class<T> acceleratorEnumClass): accelerator enumeration class
                particleTransferName (java.lang.String): particle transfer name
        
            Returns:
                :class:`~cern.accsoft.commons.domain.particletransfers.ParticleTransfer`
        
            Raises:
                java.lang.IllegalArgumentException: if accelerator or particle transfer doesn't exist
        
        
        """
        ...

_CodeEntityConverter__E = typing.TypeVar('_CodeEntityConverter__E')  # <E>
class CodeEntityConverter(typing.Generic[_CodeEntityConverter__E]):
    """
    public class CodeEntityConverter<E> extends java.lang.Object
    
        Generic class to convert between codes and corresponding entities.
    """
    @typing.overload
    def fromCode(self, int: int) -> _CodeEntityConverter__E:
        """
        
            Parameters:
                code (long): code
        
            Returns:
                corresponding entity
        
        
            Parameters:
                code (int): code
        
            Returns:
                corresponding entity
        
        
        """
        ...
    @typing.overload
    def fromCode(self, long: int) -> _CodeEntityConverter__E: ...
    def getFromCodeMap(self) -> java.util.Map[int, _CodeEntityConverter__E]: ...
    def getToCodeMap(self) -> java.util.Map[_CodeEntityConverter__E, int]: ...
    def toCode(self, e: _CodeEntityConverter__E) -> int:
        """
        
            Parameters:
                entity (:class:`~cern.accsoft.commons.domain.util.CodeEntityConverter`): entity
        
            Returns:
                corresponding code
        
        
        """
        ...

class EnumEmulationUtils:
    """
    @Deprecated public class EnumEmulationUtils extends java.lang.Object
    
        Deprecated.
        use cern.accsoft.commons.util.enums.EnumEmulationUtil from accsoft-commons-domain
    """
    _getEnumConstants__T = typing.TypeVar('_getEnumConstants__T')  # <T>
    @staticmethod
    def getEnumConstants(class_: typing.Type[_getEnumConstants__T]) -> typing.MutableSequence[_getEnumConstants__T]:
        """
            Deprecated.
            Enum like class as supposed to have a values method. It is invoked in order to fetch the values list
        
            Parameters:
                enumClass (java.lang.Class<T> enumClass): the enum-like class
        
            Returns:
                the array of available values in this class
        
        
        """
        ...
    _getEnumConstantsMap__T = typing.TypeVar('_getEnumConstantsMap__T')  # <T>
    @staticmethod
    def getEnumConstantsMap(class_: typing.Type[_getEnumConstantsMap__T]) -> java.util.Map[str, _getEnumConstantsMap__T]:
        """
            Deprecated.
        
        """
        ...
    _valueOf__T = typing.TypeVar('_valueOf__T')  # <T>
    @staticmethod
    def valueOf(string: str, class_: typing.Type[_valueOf__T]) -> _valueOf__T:
        """
            Deprecated.
        
        """
        ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.domain.util")``.

    AccsoftDomainUtil: typing.Type[AccsoftDomainUtil]
    CodeEntityConverter: typing.Type[CodeEntityConverter]
    EnumEmulationUtils: typing.Type[EnumEmulationUtils]
