import cern.lsa.domain.settings
import java.util
import typing



class DefaultDateFormatter:
    FORMAT: typing.ClassVar[str] = ...
    @staticmethod
    def format(date: java.util.Date) -> str: ...
    @staticmethod
    def parse(string: str) -> java.util.Date: ...

class HumanReadableStrings:
    @typing.overload
    @staticmethod
    def toHumanReadable(settingsSource: cern.lsa.domain.settings.SettingsSource) -> str: ...
    @typing.overload
    @staticmethod
    def toHumanReadable(trimHeader: cern.lsa.domain.settings.TrimHeader) -> str: ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.commons.util.format")``.

    DefaultDateFormatter: typing.Type[DefaultDateFormatter]
    HumanReadableStrings: typing.Type[HumanReadableStrings]
