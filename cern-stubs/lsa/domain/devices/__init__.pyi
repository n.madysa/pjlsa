import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.particletransfers
import cern.accsoft.commons.domain.zones
import cern.accsoft.commons.util
import cern.lsa.domain.commons
import cern.lsa.domain.devices.factory
import cern.lsa.domain.devices.inca
import cern.lsa.domain.devices.spi
import cern.lsa.domain.devices.type
import com.google.common.collect
import java.io
import java.lang
import java.util
import typing



class CalibrationsRequest:
    ALL: typing.ClassVar['CalibrationsRequest'] = ...
    @staticmethod
    def builder() -> 'DefaultCalibrationsRequest.Builder': ...
    @staticmethod
    def byCalibrationNames(collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'CalibrationsRequest': ...
    @staticmethod
    def byLogicalHardwareName(string: str) -> 'CalibrationsRequest': ...
    @staticmethod
    def byParticleTransfer(particleTransfer: cern.accsoft.commons.domain.particletransfers.ParticleTransfer) -> 'CalibrationsRequest': ...
    def getCalibrationNames(self) -> java.util.Set[str]: ...
    def getLogicalHardwareName(self) -> str: ...
    def getParticleTransfer(self) -> cern.accsoft.commons.domain.particletransfers.ParticleTransfer: ...

class Device(cern.accsoft.commons.util.Named, cern.lsa.domain.commons.IdentifiedEntity, java.lang.Comparable['Device']):
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getAcceleratorZone(self) -> cern.accsoft.commons.domain.zones.AcceleratorZone: ...
    def getAlias(self) -> str: ...
    def getDescription(self) -> str: ...
    def getDeviceGroups(self) -> java.util.Set[str]: ...
    def getDeviceType(self) -> 'DeviceType': ...
    def getDeviceTypeVersion(self) -> 'DeviceTypeVersion': ...
    def getFecName(self) -> str: ...
    def getPosition(self) -> float: ...
    def getServerName(self) -> str: ...
    def getSortOrder(self) -> int: ...
    def getState(self) -> 'Device.DeviceState': ...
    def isCycleBound(self) -> bool: ...
    def isLsaImplementation(self) -> bool: ...
    def isMultiplexed(self) -> bool: ...
    class DeviceState(java.lang.Enum['Device.DeviceState'], cern.accsoft.commons.util.Named):
        OPERATIONAL: typing.ClassVar['Device.DeviceState'] = ...
        EXPERT: typing.ClassVar['Device.DeviceState'] = ...
        DEVELOPMENT: typing.ClassVar['Device.DeviceState'] = ...
        OBSOLETE: typing.ClassVar['Device.DeviceState'] = ...
        def getName(self) -> str: ...
        _valueOf_1__T = typing.TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
        @typing.overload
        @staticmethod
        def valueOf(string: str) -> 'Device.DeviceState': ...
        @typing.overload
        @staticmethod
        def valueOf(class_: typing.Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
        @staticmethod
        def values() -> typing.MutableSequence['Device.DeviceState']: ...

class DeviceFilter(cern.accsoft.commons.util.Filters.Filter[Device]):
    def __init__(self): ...
    @staticmethod
    def acceleratorZone(acceleratorZone: cern.accsoft.commons.domain.zones.AcceleratorZone) -> 'DeviceFilter': ...
    @staticmethod
    def acceleratorZoneIn(collection: typing.Union[java.util.Collection[cern.accsoft.commons.domain.zones.AcceleratorZone], typing.Sequence[cern.accsoft.commons.domain.zones.AcceleratorZone]]) -> 'DeviceFilter': ...
    def accepts(self, device: Device) -> bool: ...
    @staticmethod
    def deviceGroupName(string: str) -> 'DeviceFilter': ...
    @staticmethod
    def deviceTypeName(string: str) -> 'DeviceFilter': ...
    @staticmethod
    def deviceTypeNames(collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DeviceFilter': ...
    def setAcceleratorZone(self, acceleratorZone: cern.accsoft.commons.domain.zones.AcceleratorZone) -> 'DeviceFilter': ...
    def setAcceleratorZones(self, collection: typing.Union[java.util.Collection[cern.accsoft.commons.domain.zones.AcceleratorZone], typing.Sequence[cern.accsoft.commons.domain.zones.AcceleratorZone]]) -> 'DeviceFilter': ...
    def setDeviceGroupName(self, string: str) -> 'DeviceFilter': ...
    def setDeviceGroupNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DeviceFilter': ...
    def setDeviceType(self, deviceType: 'DeviceType') -> 'DeviceFilter': ...
    def setDeviceTypeName(self, string: str) -> 'DeviceFilter': ...
    def setDeviceTypeNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DeviceFilter': ...
    def setDeviceTypeVersions(self, collection: typing.Union[java.util.Collection['DeviceTypeVersion'], typing.Sequence['DeviceTypeVersion']]) -> 'DeviceFilter': ...
    def setDeviceTypes(self, collection: typing.Union[java.util.Collection['DeviceType'], typing.Sequence['DeviceType']]) -> 'DeviceFilter': ...

class DeviceGroup(cern.accsoft.commons.util.Named, cern.lsa.domain.commons.IdentifiedEntity):
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getChildGroups(self) -> java.util.Set['DeviceGroup']: ...
    def getCreateTime(self) -> java.util.Date: ...
    def getCreator(self) -> str: ...
    def getDisplayName(self) -> str: ...
    def getModifier(self) -> str: ...
    def getModifyTime(self) -> java.util.Date: ...
    def getType(self) -> 'DeviceGroupType': ...
    def isOperational(self) -> bool: ...

class DeviceGroupFilter(cern.accsoft.commons.util.Filters.Filter[DeviceGroup]):
    def __init__(self): ...
    @staticmethod
    def accelerator(accelerator: cern.accsoft.commons.domain.Accelerator) -> 'DeviceGroupFilter': ...
    def accepts(self, deviceGroup: DeviceGroup) -> bool: ...
    def setAccelerator(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> 'DeviceGroupFilter': ...
    def setType(self, deviceGroupType: 'DeviceGroupType') -> 'DeviceGroupFilter': ...
    def setTypes(self, collection: typing.Union[java.util.Collection['DeviceGroupType'], typing.Sequence['DeviceGroupType']]) -> 'DeviceGroupFilter': ...
    @staticmethod
    def type(deviceGroupType: 'DeviceGroupType') -> 'DeviceGroupFilter': ...
    @staticmethod
    def typeIn(collection: typing.Union[java.util.Collection['DeviceGroupType'], typing.Sequence['DeviceGroupType']]) -> 'DeviceGroupFilter': ...

class DeviceGroupType(cern.accsoft.commons.util.Named):
    def getDescription(self) -> str: ...

class DeviceGroups:
    CHILD_GROUPS: typing.ClassVar[cern.accsoft.commons.util.Mappers.Mapper] = ...
    def __init__(self): ...
    @staticmethod
    def filterGroupsByType(collection: typing.Union[java.util.Collection[DeviceGroup], typing.Sequence[DeviceGroup]], deviceGroupType: DeviceGroupType) -> java.util.Set[DeviceGroup]: ...
    @staticmethod
    def getAllChildDeviceGroups(collection: typing.Union[java.util.Collection[DeviceGroup], typing.Sequence[DeviceGroup]]) -> java.util.Set[DeviceGroup]: ...
    @staticmethod
    def getDirectChildDeviceGroups(collection: typing.Union[java.util.Collection[DeviceGroup], typing.Sequence[DeviceGroup]]) -> java.util.Set[DeviceGroup]: ...

class DeviceGroupsRequest:
    @staticmethod
    def builder() -> cern.lsa.domain.devices.factory.DeviceGroupsRequestBuilder: ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getDeviceGroupNamePattern(self) -> str: ...
    def getDeviceGroupNames(self) -> java.util.Set[str]: ...
    def getDeviceGroupTypes(self) -> java.util.Set[DeviceGroupType]: ...

class DeviceMetaTypeEnum(java.lang.Enum['DeviceMetaTypeEnum'], cern.accsoft.commons.util.Named):
    ACTUAL: typing.ClassVar['DeviceMetaTypeEnum'] = ...
    LOGICAL: typing.ClassVar['DeviceMetaTypeEnum'] = ...
    BEAM: typing.ClassVar['DeviceMetaTypeEnum'] = ...
    ALL: typing.ClassVar['DeviceMetaTypeEnum'] = ...
    NONE: typing.ClassVar['DeviceMetaTypeEnum'] = ...
    def getMetaType(self) -> str: ...
    def getName(self) -> str: ...
    def toString(self) -> str: ...
    _valueOf_2__T = typing.TypeVar('_valueOf_2__T', bound=java.lang.Enum)  # <T>
    @typing.overload
    @staticmethod
    def valueOf(char: str) -> 'DeviceMetaTypeEnum': ...
    @typing.overload
    @staticmethod
    def valueOf(string: str) -> 'DeviceMetaTypeEnum': ...
    @typing.overload
    @staticmethod
    def valueOf(class_: typing.Type[_valueOf_2__T], string: str) -> _valueOf_2__T: ...
    @staticmethod
    def values() -> typing.MutableSequence['DeviceMetaTypeEnum']: ...

class DeviceType(cern.accsoft.commons.util.Named, cern.lsa.domain.commons.IdentifiedEntity, java.lang.Comparable['DeviceType']):
    def getDescription(self) -> str: ...
    def getMetaType(self) -> DeviceMetaTypeEnum: ...
    def getVersions(self) -> java.util.SortedSet['DeviceTypeVersion']: ...

class DeviceTypeImplementation(java.lang.Enum['DeviceTypeImplementation'], cern.accsoft.commons.util.Named):
    HARDWARE: typing.ClassVar['DeviceTypeImplementation'] = ...
    FESA2: typing.ClassVar['DeviceTypeImplementation'] = ...
    FESA3: typing.ClassVar['DeviceTypeImplementation'] = ...
    VIRTUAL: typing.ClassVar['DeviceTypeImplementation'] = ...
    GM: typing.ClassVar['DeviceTypeImplementation'] = ...
    DEVACC: typing.ClassVar['DeviceTypeImplementation'] = ...
    LSA: typing.ClassVar['DeviceTypeImplementation'] = ...
    FGC: typing.ClassVar['DeviceTypeImplementation'] = ...
    def getName(self) -> str: ...
    _valueOf_1__T = typing.TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @typing.overload
    @staticmethod
    def valueOf(string: str) -> 'DeviceTypeImplementation': ...
    @typing.overload
    @staticmethod
    def valueOf(class_: typing.Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @staticmethod
    def values() -> typing.MutableSequence['DeviceTypeImplementation']: ...

class DeviceTypeVersion(cern.lsa.domain.commons.IdentifiedEntity, java.lang.Comparable['DeviceTypeVersion']):
    def getDeviceType(self) -> DeviceType: ...
    def getImplementation(self) -> DeviceTypeImplementation: ...
    def getVersionNumber(self) -> 'DeviceTypeVersionNumber': ...

class DeviceTypeVersionNumber(java.lang.Comparable['DeviceTypeVersionNumber']):
    def getMajor(self) -> int: ...
    def getMinor(self) -> int: ...

class DeviceTypes:
    def __init__(self): ...
    @staticmethod
    def getLatestVersion(deviceType: DeviceType) -> DeviceTypeVersion: ...
    @staticmethod
    def toDeviceTypeVersionByDeviceTypeMap(collection: typing.Union[java.util.Collection[DeviceTypeVersion], typing.Sequence[DeviceTypeVersion]]) -> java.util.Map[DeviceType, java.util.SortedSet[DeviceTypeVersion]]: ...
    @staticmethod
    def toDeviceTypeVersions(collection: typing.Union[java.util.Collection[DeviceType], typing.Sequence[DeviceType]]) -> java.util.Set[DeviceTypeVersion]: ...
    @staticmethod
    def toDeviceTypes(collection: typing.Union[java.util.Collection[DeviceTypeVersion], typing.Sequence[DeviceTypeVersion]]) -> java.util.Set[DeviceType]: ...

class DeviceTypesRequest:
    @staticmethod
    def builder() -> cern.lsa.domain.devices.factory.DeviceTypesRequestBuilder: ...
    def check(self) -> None: ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getAllTypesRequested(self) -> bool: ...
    def getDeviceTypeImplementations(self) -> java.util.Set[DeviceTypeImplementation]: ...
    def getDeviceTypeNamePattern(self) -> str: ...
    def getDeviceTypeNames(self) -> java.util.Set[str]: ...
    def getDeviceTypeVersionNumber(self) -> DeviceTypeVersionNumber: ...

class Devices:
    DEVICE_TYPES: typing.ClassVar[cern.accsoft.commons.util.Mappers.Mapper] = ...
    DEVICE_TYPE_NAMES: typing.ClassVar[cern.accsoft.commons.util.Mappers.Mapper] = ...
    DEVICE_TYPE_VERSIONS: typing.ClassVar[cern.accsoft.commons.util.Mappers.Mapper] = ...
    FRONT_END_NAMES: typing.ClassVar[cern.accsoft.commons.util.Mappers.Mapper] = ...
    DEVICE_GROUP_NAMES: typing.ClassVar[cern.accsoft.commons.util.Mappers.Mapper] = ...
    def __init__(self): ...
    @staticmethod
    def deviceGroupNameIn(collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> cern.accsoft.commons.util.Filters.Filter[Device]: ...
    @staticmethod
    def filterDevicesByGroupNames(collection: typing.Union[java.util.Collection[Device], typing.Sequence[Device]], collection2: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> java.util.Set[Device]: ...
    @staticmethod
    def filterDevicesNotAssignedToAnyGroup(collection: typing.Union[java.util.Collection[Device], typing.Sequence[Device]]) -> java.util.Set[Device]: ...
    @staticmethod
    def filterDevicesNotAssignedToGroup(collection: typing.Union[java.util.Collection[Device], typing.Sequence[Device]], collection2: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> java.util.Set[Device]: ...
    @staticmethod
    def findGroupsContainingDeviceByType(collection: typing.Union[java.util.Collection[DeviceGroup], typing.Sequence[DeviceGroup]], deviceGroupType: DeviceGroupType, device2: Device) -> java.util.Set[DeviceGroup]: ...
    @staticmethod
    def getDeviceGroupNames(set: java.util.Set[Device]) -> java.util.Set[str]: ...
    @staticmethod
    def getDeviceTypeNames(collection: typing.Union[java.util.Collection[Device], typing.Sequence[Device]]) -> java.util.Set[str]: ...
    @staticmethod
    def getDeviceTypeVersions(collection: typing.Union[java.util.Collection[Device], typing.Sequence[Device]]) -> java.util.Set[DeviceTypeVersion]: ...
    @staticmethod
    def getDeviceTypes(collection: typing.Union[java.util.Collection[Device], typing.Sequence[Device]]) -> java.util.Set[DeviceType]: ...
    @staticmethod
    def getDeviceTypesByDeviceGroup(set: java.util.Set[Device], string: str) -> java.util.Set[DeviceType]: ...
    @staticmethod
    def getFrontEndNames(set: java.util.Set[Device]) -> java.util.Set[str]: ...
    @staticmethod
    def groupDevicesByDeviceGroup(collection: typing.Union[java.util.Collection[Device], typing.Sequence[Device]]) -> java.util.Map[str, java.util.Set[Device]]: ...

class DevicesRequest:
    @staticmethod
    def builder() -> cern.lsa.domain.devices.factory.DevicesRequestBuilder: ...
    def existInLsaOnly(self) -> bool: ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getAcceleratorZones(self) -> java.util.Set[cern.accsoft.commons.domain.zones.AcceleratorZone]: ...
    def getDeviceAliases(self) -> java.util.Set[str]: ...
    def getDeviceGroupIds(self) -> java.util.Set[int]: ...
    def getDeviceGroupNames(self) -> java.util.Set[str]: ...
    def getDeviceNamePattern(self) -> str: ...
    def getDeviceNames(self) -> java.util.Set[str]: ...
    def getDeviceTypeNamePattern(self) -> str: ...
    def getDeviceTypeNames(self) -> java.util.Set[str]: ...
    def getDeviceTypeVersions(self) -> java.util.Set[DeviceTypeVersion]: ...
    def getElementName(self) -> str: ...
    def getFecNames(self) -> java.util.Set[str]: ...
    def getMetaType(self) -> DeviceMetaTypeEnum: ...
    def getParticleTransfers(self) -> java.util.Set[cern.accsoft.commons.domain.particletransfers.ParticleTransfer]: ...
    def getServerNames(self) -> java.util.Set[str]: ...
    def isMultiplexed(self) -> bool: ...

class PowerConverterNestedCircuitInfo:
    @staticmethod
    def builder() -> 'DefaultPowerConverterNestedCircuitInfo.Builder': ...
    def getActualDeviceName(self) -> str: ...
    def getCoefficient(self) -> int: ...
    def getLogicalDeviceName(self) -> str: ...

class PowerConverterNestedCircuitInfosRequest:
    @staticmethod
    def builder() -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
    @staticmethod
    def byActualDeviceNames(collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'PowerConverterNestedCircuitInfosRequest': ...
    @staticmethod
    def byActualDevices(collection: typing.Union[java.util.Collection[Device], typing.Sequence[Device]]) -> 'PowerConverterNestedCircuitInfosRequest': ...
    @staticmethod
    def byLogicalDeviceNames(collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'PowerConverterNestedCircuitInfosRequest': ...
    @staticmethod
    def byLogicalDevices(collection: typing.Union[java.util.Collection[Device], typing.Sequence[Device]]) -> 'PowerConverterNestedCircuitInfosRequest': ...
    def check(self) -> None: ...
    @staticmethod
    def forSingleLogicalActualRelation(string: str, string2: str) -> 'PowerConverterNestedCircuitInfosRequest': ...
    def getActualDeviceNames(self) -> java.util.List[str]: ...
    def getLogicalDeviceNames(self) -> java.util.List[str]: ...

class DefaultCalibrationsRequest(CalibrationsRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultCalibrationsRequest.Builder': ...
    @staticmethod
    def copyOf(calibrationsRequest: CalibrationsRequest) -> 'DefaultCalibrationsRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getCalibrationNames(self) -> com.google.common.collect.ImmutableSet[str]: ...
    def getLogicalHardwareName(self) -> str: ...
    def getParticleTransfer(self) -> cern.accsoft.commons.domain.particletransfers.ParticleTransfer: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    @typing.overload
    def withCalibrationNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultCalibrationsRequest': ...
    @typing.overload
    def withCalibrationNames(self, *string: str) -> 'DefaultCalibrationsRequest': ...
    def withLogicalHardwareName(self, string: str) -> 'DefaultCalibrationsRequest': ...
    def withParticleTransfer(self, particleTransfer: cern.accsoft.commons.domain.particletransfers.ParticleTransfer) -> 'DefaultCalibrationsRequest': ...
    class Builder:
        def addAllCalibrationNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultCalibrationsRequest.Builder': ...
        def addCalibrationName(self, string: str) -> 'DefaultCalibrationsRequest.Builder': ...
        def addCalibrationNames(self, *string: str) -> 'DefaultCalibrationsRequest.Builder': ...
        def build(self) -> 'DefaultCalibrationsRequest': ...
        def calibrationNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultCalibrationsRequest.Builder': ...
        def from_(self, calibrationsRequest: CalibrationsRequest) -> 'DefaultCalibrationsRequest.Builder': ...
        def logicalHardwareName(self, string: str) -> 'DefaultCalibrationsRequest.Builder': ...
        def particleTransfer(self, particleTransfer: cern.accsoft.commons.domain.particletransfers.ParticleTransfer) -> 'DefaultCalibrationsRequest.Builder': ...

class DefaultDeviceTypesRequest(DeviceTypesRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultDeviceTypesRequest.Builder': ...
    @staticmethod
    def copyOf(deviceTypesRequest: DeviceTypesRequest) -> 'DefaultDeviceTypesRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getAllTypesRequested(self) -> bool: ...
    def getDeviceTypeImplementations(self) -> java.util.Set[DeviceTypeImplementation]: ...
    def getDeviceTypeNamePattern(self) -> str: ...
    def getDeviceTypeNames(self) -> java.util.Set[str]: ...
    def getDeviceTypeVersionNumber(self) -> DeviceTypeVersionNumber: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withAccelerator(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> 'DefaultDeviceTypesRequest': ...
    def withAllTypesRequested(self, boolean: bool) -> 'DefaultDeviceTypesRequest': ...
    @typing.overload
    def withDeviceTypeImplementations(self, *deviceTypeImplementation: DeviceTypeImplementation) -> 'DefaultDeviceTypesRequest': ...
    @typing.overload
    def withDeviceTypeImplementations(self, iterable: java.lang.Iterable[DeviceTypeImplementation]) -> 'DefaultDeviceTypesRequest': ...
    def withDeviceTypeNamePattern(self, string: str) -> 'DefaultDeviceTypesRequest': ...
    @typing.overload
    def withDeviceTypeNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultDeviceTypesRequest': ...
    @typing.overload
    def withDeviceTypeNames(self, *string: str) -> 'DefaultDeviceTypesRequest': ...
    def withDeviceTypeVersionNumber(self, deviceTypeVersionNumber: DeviceTypeVersionNumber) -> 'DefaultDeviceTypesRequest': ...
    class Builder:
        def accelerator(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> 'DefaultDeviceTypesRequest.Builder': ...
        def addAllDeviceTypeImplementations(self, iterable: java.lang.Iterable[DeviceTypeImplementation]) -> 'DefaultDeviceTypesRequest.Builder': ...
        def addAllDeviceTypeNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultDeviceTypesRequest.Builder': ...
        def addDeviceTypeImplementation(self, deviceTypeImplementation: DeviceTypeImplementation) -> 'DefaultDeviceTypesRequest.Builder': ...
        def addDeviceTypeImplementations(self, *deviceTypeImplementation: DeviceTypeImplementation) -> 'DefaultDeviceTypesRequest.Builder': ...
        def addDeviceTypeName(self, string: str) -> 'DefaultDeviceTypesRequest.Builder': ...
        def addDeviceTypeNames(self, *string: str) -> 'DefaultDeviceTypesRequest.Builder': ...
        def allTypesRequested(self, boolean: bool) -> 'DefaultDeviceTypesRequest.Builder': ...
        def build(self) -> 'DefaultDeviceTypesRequest': ...
        def deviceTypeImplementations(self, iterable: java.lang.Iterable[DeviceTypeImplementation]) -> 'DefaultDeviceTypesRequest.Builder': ...
        def deviceTypeNamePattern(self, string: str) -> 'DefaultDeviceTypesRequest.Builder': ...
        def deviceTypeNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultDeviceTypesRequest.Builder': ...
        def deviceTypeVersionNumber(self, deviceTypeVersionNumber: DeviceTypeVersionNumber) -> 'DefaultDeviceTypesRequest.Builder': ...
        def from_(self, deviceTypesRequest: DeviceTypesRequest) -> 'DefaultDeviceTypesRequest.Builder': ...

class DefaultPowerConverterNestedCircuitInfo(PowerConverterNestedCircuitInfo, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultPowerConverterNestedCircuitInfo.Builder': ...
    @staticmethod
    def copyOf(powerConverterNestedCircuitInfo: PowerConverterNestedCircuitInfo) -> 'DefaultPowerConverterNestedCircuitInfo': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getActualDeviceName(self) -> str: ...
    def getCoefficient(self) -> int: ...
    def getLogicalDeviceName(self) -> str: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withActualDeviceName(self, string: str) -> 'DefaultPowerConverterNestedCircuitInfo': ...
    def withCoefficient(self, int: int) -> 'DefaultPowerConverterNestedCircuitInfo': ...
    def withLogicalDeviceName(self, string: str) -> 'DefaultPowerConverterNestedCircuitInfo': ...
    class Builder:
        def actualDeviceName(self, string: str) -> 'DefaultPowerConverterNestedCircuitInfo.Builder': ...
        def build(self) -> 'DefaultPowerConverterNestedCircuitInfo': ...
        def coefficient(self, int: int) -> 'DefaultPowerConverterNestedCircuitInfo.Builder': ...
        def from_(self, powerConverterNestedCircuitInfo: PowerConverterNestedCircuitInfo) -> 'DefaultPowerConverterNestedCircuitInfo.Builder': ...
        def logicalDeviceName(self, string: str) -> 'DefaultPowerConverterNestedCircuitInfo.Builder': ...

class DefaultPowerConverterNestedCircuitInfosRequest(PowerConverterNestedCircuitInfosRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
    @staticmethod
    def copyOf(powerConverterNestedCircuitInfosRequest: PowerConverterNestedCircuitInfosRequest) -> 'DefaultPowerConverterNestedCircuitInfosRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getActualDeviceNames(self) -> java.util.List[str]: ...
    def getLogicalDeviceNames(self) -> java.util.List[str]: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    @typing.overload
    def withActualDeviceNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultPowerConverterNestedCircuitInfosRequest': ...
    @typing.overload
    def withActualDeviceNames(self, *string: str) -> 'DefaultPowerConverterNestedCircuitInfosRequest': ...
    @typing.overload
    def withLogicalDeviceNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultPowerConverterNestedCircuitInfosRequest': ...
    @typing.overload
    def withLogicalDeviceNames(self, *string: str) -> 'DefaultPowerConverterNestedCircuitInfosRequest': ...
    class Builder:
        def actualDeviceNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
        def addActualDeviceName(self, string: str) -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
        def addActualDeviceNames(self, *string: str) -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
        def addAllActualDeviceNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
        def addAllLogicalDeviceNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
        def addLogicalDeviceName(self, string: str) -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
        def addLogicalDeviceNames(self, *string: str) -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
        def build(self) -> 'DefaultPowerConverterNestedCircuitInfosRequest': ...
        def from_(self, powerConverterNestedCircuitInfosRequest: PowerConverterNestedCircuitInfosRequest) -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...
        def logicalDeviceNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultPowerConverterNestedCircuitInfosRequest.Builder': ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.devices")``.

    CalibrationsRequest: typing.Type[CalibrationsRequest]
    DefaultCalibrationsRequest: typing.Type[DefaultCalibrationsRequest]
    DefaultDeviceTypesRequest: typing.Type[DefaultDeviceTypesRequest]
    DefaultPowerConverterNestedCircuitInfo: typing.Type[DefaultPowerConverterNestedCircuitInfo]
    DefaultPowerConverterNestedCircuitInfosRequest: typing.Type[DefaultPowerConverterNestedCircuitInfosRequest]
    Device: typing.Type[Device]
    DeviceFilter: typing.Type[DeviceFilter]
    DeviceGroup: typing.Type[DeviceGroup]
    DeviceGroupFilter: typing.Type[DeviceGroupFilter]
    DeviceGroupType: typing.Type[DeviceGroupType]
    DeviceGroups: typing.Type[DeviceGroups]
    DeviceGroupsRequest: typing.Type[DeviceGroupsRequest]
    DeviceMetaTypeEnum: typing.Type[DeviceMetaTypeEnum]
    DeviceType: typing.Type[DeviceType]
    DeviceTypeImplementation: typing.Type[DeviceTypeImplementation]
    DeviceTypeVersion: typing.Type[DeviceTypeVersion]
    DeviceTypeVersionNumber: typing.Type[DeviceTypeVersionNumber]
    DeviceTypes: typing.Type[DeviceTypes]
    DeviceTypesRequest: typing.Type[DeviceTypesRequest]
    Devices: typing.Type[Devices]
    DevicesRequest: typing.Type[DevicesRequest]
    PowerConverterNestedCircuitInfo: typing.Type[PowerConverterNestedCircuitInfo]
    PowerConverterNestedCircuitInfosRequest: typing.Type[PowerConverterNestedCircuitInfosRequest]
    factory: cern.lsa.domain.devices.factory.__module_protocol__
    inca: cern.lsa.domain.devices.inca.__module_protocol__
    spi: cern.lsa.domain.devices.spi.__module_protocol__
    type: cern.lsa.domain.devices.type.__module_protocol__
