import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.beams
import cern.accsoft.commons.domain.particletransfers
import cern.accsoft.commons.domain.zones
import cern.accsoft.commons.util
import cern.accsoft.commons.value
import cern.lsa.domain.commons
import cern.lsa.domain.devices
import cern.lsa.domain.optics.factory
import cern.lsa.domain.optics.spi
import cern.lsa.domain.settings
import cern.lsa.domain.settings.type
import com.google.common.collect
import java.io
import java.lang
import java.util
import jpype
import typing



class BeamEnum(java.lang.Enum['BeamEnum'], cern.accsoft.commons.util.Named):
    B1: typing.ClassVar['BeamEnum'] = ...
    B2: typing.ClassVar['BeamEnum'] = ...
    B3: typing.ClassVar['BeamEnum'] = ...
    B4: typing.ClassVar['BeamEnum'] = ...
    def getName(self) -> str: ...
    @staticmethod
    def toBeamEnum(string: str) -> 'BeamEnum': ...
    _valueOf_1__T = typing.TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @typing.overload
    @staticmethod
    def valueOf(string: str) -> 'BeamEnum': ...
    @typing.overload
    @staticmethod
    def valueOf(class_: typing.Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @staticmethod
    def values() -> typing.MutableSequence['BeamEnum']: ...

class Calibration(cern.accsoft.commons.util.Named):
    def getCalibrationFunctionByType(self, calibrationFunctionTypes: 'CalibrationFunctionTypes') -> cern.accsoft.commons.value.ImmutableDiscreteFunction: ...
    def getCalibrationFunctionMap(self) -> java.util.Map['CalibrationFunctionTypes', cern.accsoft.commons.value.ImmutableDiscreteFunction]: ...
    def getCreationDate(self) -> java.util.Date: ...
    def getFidelModelId(self) -> int: ...

class CalibrationException(java.lang.Exception):
    @typing.overload
    def __init__(self, string: str): ...
    @typing.overload
    def __init__(self, string: str, throwable: java.lang.Throwable): ...

class CalibrationFunctionTypes(java.lang.Enum['CalibrationFunctionTypes'], cern.accsoft.commons.util.Named):
    B_FIELD: typing.ClassVar['CalibrationFunctionTypes'] = ...
    BL_FIELD: typing.ClassVar['CalibrationFunctionTypes'] = ...
    SLOPE: typing.ClassVar['CalibrationFunctionTypes'] = ...
    CURRENT2INDUCTANCE: typing.ClassVar['CalibrationFunctionTypes'] = ...
    CURRENT2COUPLING_INDUCTANCE: typing.ClassVar['CalibrationFunctionTypes'] = ...
    MAG_INTFIELD2CURRENT: typing.ClassVar['CalibrationFunctionTypes'] = ...
    EL_INTFIELD2VOLTAGE: typing.ClassVar['CalibrationFunctionTypes'] = ...
    LOCAL_FIELD2CURRENT: typing.ClassVar['CalibrationFunctionTypes'] = ...
    CURRENT2HALL_VOLTAGE: typing.ClassVar['CalibrationFunctionTypes'] = ...
    B_FIELD_RDOWN: typing.ClassVar['CalibrationFunctionTypes'] = ...
    MAG_INTFIELD2VOLTAGE: typing.ClassVar['CalibrationFunctionTypes'] = ...
    BL_FIELD_RDOWN: typing.ClassVar['CalibrationFunctionTypes'] = ...
    def getFunctionTypeName(self) -> str: ...
    def getName(self) -> str: ...
    @staticmethod
    def getTypeForName(string: str) -> 'CalibrationFunctionTypes': ...
    _valueOf_1__T = typing.TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @typing.overload
    @staticmethod
    def valueOf(string: str) -> 'CalibrationFunctionTypes': ...
    @typing.overload
    @staticmethod
    def valueOf(class_: typing.Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @staticmethod
    def values() -> typing.MutableSequence['CalibrationFunctionTypes']: ...

class ChromaticModel(cern.accsoft.commons.util.Named):
    def getAlphaH(self) -> float: ...
    def getAlphaV(self) -> float: ...
    def getBetaH(self) -> float: ...
    def getBetaV(self) -> float: ...
    def getBsedd(self) -> float: ...
    def getBsrem(self) -> float: ...
    def getChcon(self) -> float: ...
    def getChedd(self) -> float: ...
    def getChrem(self) -> float: ...
    def getCvcon(self) -> float: ...
    def getCvedd(self) -> float: ...
    def getCvrem(self) -> float: ...
    def getGammaH(self) -> float: ...
    def getGammaV(self) -> float: ...
    def getLatticeChromaH(self) -> float: ...
    def getLatticeChromaV(self) -> float: ...

class ElementBase(cern.accsoft.commons.util.Named, cern.lsa.domain.commons.IdentifiedEntity):
    def getAcceleratorZone(self) -> cern.accsoft.commons.domain.zones.AcceleratorZone: ...
    def getLength(self) -> float: ...
    def getMadParent(self) -> str: ...
    def getPlane(self) -> 'ElementPlane': ...
    def getPosition(self) -> float: ...
    def getType(self) -> 'ElementType': ...

class ElementPlane(java.lang.Enum['ElementPlane'], cern.accsoft.commons.util.Named):
    VERTICAL: typing.ClassVar['ElementPlane'] = ...
    HORIZONTAL: typing.ClassVar['ElementPlane'] = ...
    BOTH: typing.ClassVar['ElementPlane'] = ...
    NONE: typing.ClassVar['ElementPlane'] = ...
    @staticmethod
    def findPlaneBySymbol(char: str) -> 'ElementPlane': ...
    def getName(self) -> str: ...
    def getSymbol(self) -> str: ...
    _valueOf_1__T = typing.TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @typing.overload
    @staticmethod
    def valueOf(string: str) -> 'ElementPlane': ...
    @typing.overload
    @staticmethod
    def valueOf(class_: typing.Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @staticmethod
    def values() -> typing.MutableSequence['ElementPlane']: ...

class ElementType(java.lang.Enum['ElementType'], cern.accsoft.commons.util.Named):
    DRIFT: typing.ClassVar['ElementType'] = ...
    HKICKER: typing.ClassVar['ElementType'] = ...
    HMONITOR: typing.ClassVar['ElementType'] = ...
    INSTRUMENT: typing.ClassVar['ElementType'] = ...
    IP: typing.ClassVar['ElementType'] = ...
    KICKER: typing.ClassVar['ElementType'] = ...
    MARKER: typing.ClassVar['ElementType'] = ...
    PLACEHOLDER: typing.ClassVar['ElementType'] = ...
    MONITOR: typing.ClassVar['ElementType'] = ...
    MULTIPOLE: typing.ClassVar['ElementType'] = ...
    OCTUPOLE: typing.ClassVar['ElementType'] = ...
    QUADRUPOLE: typing.ClassVar['ElementType'] = ...
    RBEND: typing.ClassVar['ElementType'] = ...
    RCOLLIMATOR: typing.ClassVar['ElementType'] = ...
    RFCAVITY: typing.ClassVar['ElementType'] = ...
    SBEND: typing.ClassVar['ElementType'] = ...
    SEXTUPOLE: typing.ClassVar['ElementType'] = ...
    SOLENOID: typing.ClassVar['ElementType'] = ...
    TKICKER: typing.ClassVar['ElementType'] = ...
    VKICKER: typing.ClassVar['ElementType'] = ...
    VMONITOR: typing.ClassVar['ElementType'] = ...
    def getName(self) -> str: ...
    _valueOf_1__T = typing.TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @typing.overload
    @staticmethod
    def valueOf(string: str) -> 'ElementType': ...
    @typing.overload
    @staticmethod
    def valueOf(class_: typing.Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @staticmethod
    def values() -> typing.MutableSequence['ElementType']: ...

class ElementsRequest:
    @staticmethod
    def builder() -> cern.lsa.domain.optics.factory.ElementsRequestBuilder: ...
    def check(self) -> None: ...
    def excludeObsolete(self) -> bool: ...
    def getAcceleratorZone(self) -> cern.accsoft.commons.domain.zones.AcceleratorZone: ...
    def getElementNames(self) -> java.util.Set[str]: ...
    def getLogicalHwNames(self) -> java.util.Set[str]: ...
    def getParticleTransfer(self) -> cern.accsoft.commons.domain.particletransfers.ParticleTransfer: ...
    def getSteering(self) -> bool: ...
    def getTypes(self) -> java.util.Set[ElementType]: ...

class LogicalHardware(cern.lsa.domain.devices.Device):
    def getCalibrationName(self) -> str: ...
    def getCalibrationSign(self) -> int: ...
    def getLinkRuleName(self) -> str: ...
    def getLtot(self) -> float: ...
    def getMadStrengthName(self) -> str: ...
    def getMadStrengthType(self) -> str: ...
    def getMagnetType(self) -> str: ...
    def getMagneticLength(self) -> float: ...
    def getOpTemp(self) -> float: ...
    def getPlane(self) -> ElementPlane: ...
    def getRtot(self) -> float: ...
    def getRtotMeasured(self) -> float: ...
    def getTau(self) -> float: ...

class MeasuredTwiss:
    def getAlfxError(self) -> float: ...
    def getAlfxMeas(self) -> float: ...
    def getAlfyError(self) -> float: ...
    def getAlfyMeas(self) -> float: ...
    def getBetxError(self) -> float: ...
    def getBetxMeas(self) -> float: ...
    def getBetyError(self) -> float: ...
    def getBetyMeas(self) -> float: ...
    def getDxError(self) -> float: ...
    def getDxMeas(self) -> float: ...
    def getDyError(self) -> float: ...
    def getDyMeas(self) -> float: ...
    def getEnergy(self) -> float: ...
    def getMuxError(self) -> float: ...
    def getMuxMeas(self) -> float: ...
    def getMuyError(self) -> float: ...
    def getMuyMeas(self) -> float: ...
    def getTimestamp(self) -> java.util.Date: ...
    def getTwiss(self) -> 'Twiss': ...

class Optic(cern.accsoft.commons.util.Named, cern.lsa.domain.commons.IdentifiedEntity):
    def getBaseStrengthFile(self) -> str: ...
    def getCreationDate(self) -> java.util.Date: ...
    def getModelUri(self) -> str: ...
    def getOpticParameters(self) -> java.util.Map[str, float]: ...
    def getOpticStrengths(self) -> java.util.List['OpticStrength']: ...
    def getOverrideStrengthFile(self) -> str: ...
    def getParameterNames(self) -> java.util.Set[str]: ...
    def getParameterValue(self, string: str) -> float: ...
    def getParticleTransfer(self) -> cern.accsoft.commons.domain.particletransfers.ParticleTransfer: ...
    def setName(self, string: str) -> None: ...

class OpticStrength:
    @staticmethod
    def builder() -> cern.lsa.domain.optics.spi.OpticStrengthImpl.Builder: ...
    def getBeam(self) -> str: ...
    def getLogicalHardwareName(self) -> str: ...
    def getStrength(self) -> float: ...
    def getStrengthL(self) -> float: ...

class Optics(java.io.Serializable):
    def __init__(self, set: java.util.Set[Optic]): ...
    def getBeam(self, stringArray: typing.Union[typing.List[str], jpype.JArray], string2: str) -> typing.MutableSequence[str]: ...
    def getOptic(self, string: str) -> Optic: ...
    def getOpticNames(self) -> java.util.Set[str]: ...
    def getOpticParameterValues(self, list: java.util.List[str], string: str) -> typing.MutableSequence[float]: ...
    def getStrength(self, list: java.util.List[str], string: str) -> typing.MutableSequence[float]: ...
    def getStrengthL(self, list: java.util.List[str], string: str) -> typing.MutableSequence[float]: ...
    def isParameterDefined(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]], string: str) -> bool: ...
    def size(self) -> int: ...

class OpticsRequest:
    @staticmethod
    def builder() -> cern.lsa.domain.optics.factory.OpticsRequestBuilder: ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator: ...
    def getBeamProcessTypeNames(self) -> java.util.Set[str]: ...
    def getOpticIds(self) -> java.util.Set[int]: ...
    def getParticleTransfer(self) -> cern.accsoft.commons.domain.particletransfers.ParticleTransfer: ...

class OpticsTable(java.lang.Iterable['OpticsTableItem']):
    def getBeamProcessTypeName(self) -> str: ...
    def getErrorMessages(self) -> java.util.Set[str]: ...
    def getLength(self) -> int: ...
    def getOpticNames(self) -> java.util.List[str]: ...
    def getOpticsTableItemByIndex(self, int: int) -> 'OpticsTableItem': ...
    def getOpticsTableItemByTime(self, double: float) -> 'OpticsTableItem': ...
    def getOpticsTableItems(self) -> java.util.List['OpticsTableItem']: ...
    def getSize(self) -> int: ...
    def isEmpty(self) -> bool: ...
    def isValid(self) -> bool: ...

class OpticsTableItem:
    def clone(self) -> 'OpticsTableItem': ...
    def getBeamProcessTypeName(self) -> str: ...
    def getEnergy(self) -> float: ...
    def getOpticId(self) -> int: ...
    def getOpticName(self) -> str: ...
    def getTime(self) -> int: ...

class OpticsTables:
    def __init__(self): ...
    @staticmethod
    def getEnergyFunction(opticsTable: OpticsTable) -> cern.accsoft.commons.value.ImmutableDiscreteFunction: ...
    @staticmethod
    def getOpticTimesCoordinates(opticsTable: OpticsTable) -> typing.MutableSequence[float]: ...

class OpticsTablesRequest:
    @staticmethod
    def builder() -> 'DefaultOpticsTablesRequest.Builder': ...
    @staticmethod
    def byBeamProcess(beamProcess: cern.lsa.domain.settings.BeamProcess) -> 'OpticsTablesRequest': ...
    @staticmethod
    def byBeamProcessType(beamProcessType: cern.lsa.domain.settings.type.BeamProcessType) -> 'OpticsTablesRequest': ...
    @staticmethod
    def byBeamProcessTypeName(string: str) -> 'OpticsTablesRequest': ...
    @staticmethod
    def byBeamProcessTypeNames(collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'OpticsTablesRequest': ...
    @staticmethod
    def byBeamProcessTypes(collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.type.BeamProcessType], typing.Sequence[cern.lsa.domain.settings.type.BeamProcessType]]) -> 'OpticsTablesRequest': ...
    @staticmethod
    def byBeamProcesses(collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.BeamProcess], typing.Sequence[cern.lsa.domain.settings.BeamProcess]]) -> 'OpticsTablesRequest': ...
    @staticmethod
    def byOptic(optic: Optic) -> 'OpticsTablesRequest': ...
    @staticmethod
    def byOptics(collection: typing.Union[java.util.Collection[Optic], typing.Sequence[Optic]]) -> 'OpticsTablesRequest': ...
    @staticmethod
    def byStandAloneContext(standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> 'OpticsTablesRequest': ...
    @staticmethod
    def byStandAloneContexts(collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.StandAloneContext], typing.Sequence[cern.lsa.domain.settings.StandAloneContext]]) -> 'OpticsTablesRequest': ...
    def check(self) -> None: ...
    def getBeamProcessTypeNames(self) -> java.util.Set[str]: ...
    def getOptics(self) -> java.util.Set[Optic]: ...
    def getStandAloneContexts(self) -> java.util.Set[cern.lsa.domain.settings.StandAloneContext]: ...

class PowerConverterInfo(cern.accsoft.commons.util.Named, cern.lsa.domain.commons.IdentifiedEntity):
    def getAccelerationLimit(self) -> float: ...
    def getAdvance(self) -> float: ...
    def getBeam(self) -> str: ...
    def getDecelerationLimit(self) -> float: ...
    def getDidtMax(self) -> float: ...
    def getDidtMin(self) -> float: ...
    def getIMinOp(self) -> float: ...
    def getINom(self) -> float: ...
    def getIPNo(self) -> float: ...
    def getIUlt(self) -> float: ...
    def getLastRefDatabaseUpdate(self) -> java.util.Date: ...
    def getPoweringSubsector(self) -> str: ...
    def isPolaritySwitch(self) -> bool: ...

class PreCyclingPrescription:
    def getAttributeValue(self, string: str) -> float: ...
    def getMagnetType(self) -> str: ...
    def getPrecyclingPrescriptionMode(self) -> str: ...
    def getPrecyclingPrescriptionType(self) -> str: ...

class RFCalibration(cern.accsoft.commons.util.Named):
    def getCavityQ(self, double: float) -> float: ...
    def getCavityQ2CouplerPosFunction(self) -> cern.accsoft.commons.value.ImmutableDiscreteFunction: ...
    def getCouplerPos(self, double: float) -> float: ...
    def getCreationDate(self) -> java.util.Date: ...
    def getDeviceName(self) -> str: ...

class Twiss:
    def getAlfx(self) -> float: ...
    def getAlfy(self) -> float: ...
    def getBeam(self) -> cern.accsoft.commons.domain.beams.Beam: ...
    def getBetx(self) -> float: ...
    def getBety(self) -> float: ...
    def getDpx(self) -> float: ...
    def getDpy(self) -> float: ...
    def getDx(self) -> float: ...
    def getDy(self) -> float: ...
    def getElement(self) -> ElementBase: ...
    def getHkick(self) -> float: ...
    def getK0l(self) -> float: ...
    def getK1Sl(self) -> float: ...
    def getK1l(self) -> float: ...
    def getK2Sl(self) -> float: ...
    def getK2l(self) -> float: ...
    def getK3Sl(self) -> float: ...
    def getK3l(self) -> float: ...
    def getK4l(self) -> float: ...
    def getK5l(self) -> float: ...
    def getMux(self) -> float: ...
    def getMuy(self) -> float: ...
    def getOpticName(self) -> str: ...
    def getPx(self) -> float: ...
    def getPy(self) -> float: ...
    def getVkick(self) -> float: ...
    def getX(self) -> float: ...
    def getY(self) -> float: ...
    def updateOpticName(self, string: str) -> None: ...

class TwissFilter(cern.accsoft.commons.util.Filters.Filter[Twiss]):
    def __init__(self): ...
    def accepts(self, twiss: Twiss) -> bool: ...
    @staticmethod
    def beam(beam: cern.accsoft.commons.domain.beams.Beam) -> 'TwissFilter': ...
    @staticmethod
    def elementTypeIn(collection: typing.Union[java.util.Collection[ElementType], typing.Sequence[ElementType]]) -> 'TwissFilter': ...
    def setBeam(self, beam: cern.accsoft.commons.domain.beams.Beam) -> 'TwissFilter': ...
    def setElementTypes(self, collection: typing.Union[java.util.Collection[ElementType], typing.Sequence[ElementType]]) -> 'TwissFilter': ...

class TwissHelper:
    def __init__(self): ...
    @staticmethod
    def mapByDate(collection: typing.Union[java.util.Collection[MeasuredTwiss], typing.Sequence[MeasuredTwiss]]) -> java.util.Map[java.util.Date, java.util.Set[MeasuredTwiss]]: ...
    @staticmethod
    def mapByEnergy(collection: typing.Union[java.util.Collection[MeasuredTwiss], typing.Sequence[MeasuredTwiss]]) -> java.util.Map[float, java.util.Set[MeasuredTwiss]]: ...
    @staticmethod
    def mapByOpticName(collection: typing.Union[java.util.Collection[MeasuredTwiss], typing.Sequence[MeasuredTwiss]]) -> java.util.Map[str, java.util.Set[MeasuredTwiss]]: ...
    @staticmethod
    def mapByTwiss(collection: typing.Union[java.util.Collection[MeasuredTwiss], typing.Sequence[MeasuredTwiss]]) -> java.util.Map[Twiss, java.util.Set[MeasuredTwiss]]: ...

class Twisses:
    BEAMS: typing.ClassVar[cern.accsoft.commons.util.Mappers.Mapper] = ...
    ELEMENT_TYPES: typing.ClassVar[cern.accsoft.commons.util.Mappers.Mapper] = ...
    ELEMENT_NAMES: typing.ClassVar[cern.accsoft.commons.util.Mappers.Mapper] = ...
    def __init__(self): ...
    @staticmethod
    def getBeams(collection: typing.Union[java.util.Collection[Twiss], typing.Sequence[Twiss]]) -> java.util.Set[cern.accsoft.commons.domain.beams.Beam]: ...
    @staticmethod
    def getElementTypes(collection: typing.Union[java.util.Collection[Twiss], typing.Sequence[Twiss]]) -> java.util.Set[ElementType]: ...
    @staticmethod
    def toElementNamesMap(collection: typing.Union[java.util.Collection[Twiss], typing.Sequence[Twiss]]) -> java.util.Map[str, Twiss]: ...

class TwissesRequest:
    @staticmethod
    def builder() -> cern.lsa.domain.optics.factory.TwissesRequestBuilder: ...
    def getBeam(self) -> cern.accsoft.commons.domain.beams.Beam: ...
    def getElementNames(self) -> java.util.Set[str]: ...
    def getElementPositionRange(self) -> com.google.common.collect.Range[float]: ...
    def getElementTypes(self) -> java.util.Set[ElementType]: ...
    def getOpticName(self) -> str: ...

class DefaultElementsRequest(ElementsRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultElementsRequest.Builder': ...
    @staticmethod
    def copyOf(elementsRequest: ElementsRequest) -> 'DefaultElementsRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def excludeObsolete(self) -> bool: ...
    def getAcceleratorZone(self) -> cern.accsoft.commons.domain.zones.AcceleratorZone: ...
    def getElementNames(self) -> java.util.Set[str]: ...
    def getLogicalHwNames(self) -> java.util.Set[str]: ...
    def getParticleTransfer(self) -> cern.accsoft.commons.domain.particletransfers.ParticleTransfer: ...
    def getSteering(self) -> bool: ...
    def getTypes(self) -> java.util.Set[ElementType]: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withAcceleratorZone(self, acceleratorZone: cern.accsoft.commons.domain.zones.AcceleratorZone) -> 'DefaultElementsRequest': ...
    @typing.overload
    def withElementNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultElementsRequest': ...
    @typing.overload
    def withElementNames(self, *string: str) -> 'DefaultElementsRequest': ...
    def withExcludeObsolete(self, boolean: bool) -> 'DefaultElementsRequest': ...
    @typing.overload
    def withLogicalHwNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultElementsRequest': ...
    @typing.overload
    def withLogicalHwNames(self, *string: str) -> 'DefaultElementsRequest': ...
    def withParticleTransfer(self, particleTransfer: cern.accsoft.commons.domain.particletransfers.ParticleTransfer) -> 'DefaultElementsRequest': ...
    def withSteering(self, boolean: bool) -> 'DefaultElementsRequest': ...
    @typing.overload
    def withTypes(self, *elementType: ElementType) -> 'DefaultElementsRequest': ...
    @typing.overload
    def withTypes(self, iterable: java.lang.Iterable[ElementType]) -> 'DefaultElementsRequest': ...
    class Builder:
        def acceleratorZone(self, acceleratorZone: cern.accsoft.commons.domain.zones.AcceleratorZone) -> 'DefaultElementsRequest.Builder': ...
        def addAllElementNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultElementsRequest.Builder': ...
        def addAllLogicalHwNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultElementsRequest.Builder': ...
        def addAllTypes(self, iterable: java.lang.Iterable[ElementType]) -> 'DefaultElementsRequest.Builder': ...
        def addElementName(self, string: str) -> 'DefaultElementsRequest.Builder': ...
        def addElementNames(self, *string: str) -> 'DefaultElementsRequest.Builder': ...
        def addLogicalHwName(self, string: str) -> 'DefaultElementsRequest.Builder': ...
        def addLogicalHwNames(self, *string: str) -> 'DefaultElementsRequest.Builder': ...
        def addType(self, elementType: ElementType) -> 'DefaultElementsRequest.Builder': ...
        def addTypes(self, *elementType: ElementType) -> 'DefaultElementsRequest.Builder': ...
        def build(self) -> 'DefaultElementsRequest': ...
        def elementNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultElementsRequest.Builder': ...
        def excludeObsolete(self, boolean: bool) -> 'DefaultElementsRequest.Builder': ...
        def from_(self, elementsRequest: ElementsRequest) -> 'DefaultElementsRequest.Builder': ...
        def logicalHwNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultElementsRequest.Builder': ...
        def particleTransfer(self, particleTransfer: cern.accsoft.commons.domain.particletransfers.ParticleTransfer) -> 'DefaultElementsRequest.Builder': ...
        def steering(self, boolean: bool) -> 'DefaultElementsRequest.Builder': ...
        def types(self, iterable: java.lang.Iterable[ElementType]) -> 'DefaultElementsRequest.Builder': ...

class DefaultOpticsTablesRequest(OpticsTablesRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultOpticsTablesRequest.Builder': ...
    @staticmethod
    def copyOf(opticsTablesRequest: OpticsTablesRequest) -> 'DefaultOpticsTablesRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getBeamProcessTypeNames(self) -> java.util.Set[str]: ...
    def getOptics(self) -> java.util.Set[Optic]: ...
    def getStandAloneContexts(self) -> java.util.Set[cern.lsa.domain.settings.StandAloneContext]: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    @typing.overload
    def withBeamProcessTypeNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultOpticsTablesRequest': ...
    @typing.overload
    def withBeamProcessTypeNames(self, *string: str) -> 'DefaultOpticsTablesRequest': ...
    @typing.overload
    def withOptics(self, *optic: Optic) -> 'DefaultOpticsTablesRequest': ...
    @typing.overload
    def withOptics(self, iterable: java.lang.Iterable[Optic]) -> 'DefaultOpticsTablesRequest': ...
    @typing.overload
    def withStandAloneContexts(self, *standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> 'DefaultOpticsTablesRequest': ...
    @typing.overload
    def withStandAloneContexts(self, iterable: java.lang.Iterable[cern.lsa.domain.settings.StandAloneContext]) -> 'DefaultOpticsTablesRequest': ...
    class Builder:
        def addAllBeamProcessTypeNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultOpticsTablesRequest.Builder': ...
        def addAllOptics(self, iterable: java.lang.Iterable[Optic]) -> 'DefaultOpticsTablesRequest.Builder': ...
        def addAllStandAloneContexts(self, iterable: java.lang.Iterable[cern.lsa.domain.settings.StandAloneContext]) -> 'DefaultOpticsTablesRequest.Builder': ...
        def addBeamProcessTypeName(self, string: str) -> 'DefaultOpticsTablesRequest.Builder': ...
        def addBeamProcessTypeNames(self, *string: str) -> 'DefaultOpticsTablesRequest.Builder': ...
        def addOptic(self, optic: Optic) -> 'DefaultOpticsTablesRequest.Builder': ...
        def addOptics(self, *optic: Optic) -> 'DefaultOpticsTablesRequest.Builder': ...
        def addStandAloneContext(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> 'DefaultOpticsTablesRequest.Builder': ...
        def addStandAloneContexts(self, *standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> 'DefaultOpticsTablesRequest.Builder': ...
        def beamProcessTypeNames(self, iterable: java.lang.Iterable[str]) -> 'DefaultOpticsTablesRequest.Builder': ...
        def build(self) -> 'DefaultOpticsTablesRequest': ...
        def from_(self, opticsTablesRequest: OpticsTablesRequest) -> 'DefaultOpticsTablesRequest.Builder': ...
        def optics(self, iterable: java.lang.Iterable[Optic]) -> 'DefaultOpticsTablesRequest.Builder': ...
        def standAloneContexts(self, iterable: java.lang.Iterable[cern.lsa.domain.settings.StandAloneContext]) -> 'DefaultOpticsTablesRequest.Builder': ...

class Element(ElementBase):
    def getAffectedRings(self) -> java.util.Set[BeamEnum]: ...
    def getLogicalHwName(self) -> str: ...
    def getLogicalHwNames(self) -> java.util.Set[str]: ...
    def getSteeringPlane(self) -> ElementPlane: ...
    def isObsolete(self) -> bool: ...
    def setAffectedRings(self, set: java.util.Set[BeamEnum]) -> None: ...
    def setLength(self, double: float) -> None: ...
    def setObsolete(self, boolean: bool) -> None: ...
    def setPlane(self, elementPlane: ElementPlane) -> None: ...
    def setPosition(self, double: float) -> None: ...
    def setSteeringPlane(self, elementPlane: ElementPlane) -> None: ...
    def setType(self, elementType: ElementType) -> None: ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.optics")``.

    BeamEnum: typing.Type[BeamEnum]
    Calibration: typing.Type[Calibration]
    CalibrationException: typing.Type[CalibrationException]
    CalibrationFunctionTypes: typing.Type[CalibrationFunctionTypes]
    ChromaticModel: typing.Type[ChromaticModel]
    DefaultElementsRequest: typing.Type[DefaultElementsRequest]
    DefaultOpticsTablesRequest: typing.Type[DefaultOpticsTablesRequest]
    Element: typing.Type[Element]
    ElementBase: typing.Type[ElementBase]
    ElementPlane: typing.Type[ElementPlane]
    ElementType: typing.Type[ElementType]
    ElementsRequest: typing.Type[ElementsRequest]
    LogicalHardware: typing.Type[LogicalHardware]
    MeasuredTwiss: typing.Type[MeasuredTwiss]
    Optic: typing.Type[Optic]
    OpticStrength: typing.Type[OpticStrength]
    Optics: typing.Type[Optics]
    OpticsRequest: typing.Type[OpticsRequest]
    OpticsTable: typing.Type[OpticsTable]
    OpticsTableItem: typing.Type[OpticsTableItem]
    OpticsTables: typing.Type[OpticsTables]
    OpticsTablesRequest: typing.Type[OpticsTablesRequest]
    PowerConverterInfo: typing.Type[PowerConverterInfo]
    PreCyclingPrescription: typing.Type[PreCyclingPrescription]
    RFCalibration: typing.Type[RFCalibration]
    Twiss: typing.Type[Twiss]
    TwissFilter: typing.Type[TwissFilter]
    TwissHelper: typing.Type[TwissHelper]
    Twisses: typing.Type[Twisses]
    TwissesRequest: typing.Type[TwissesRequest]
    factory: cern.lsa.domain.optics.factory.__module_protocol__
    spi: cern.lsa.domain.optics.spi.__module_protocol__
