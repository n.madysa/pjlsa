import cern.japc.core.spi.factory
import java.util
import typing



class AbstractNonProEnvironmentServiceConfigLookup(cern.japc.core.spi.factory.ServiceConfigLookup):
    """
    public class AbstractNonProEnvironmentServiceConfigLookup extends java.lang.Object implements cern.japc.core.spi.factory.ServiceConfigLookup
    
        Should be used by both LSA server and clients to redirect japc operations to the a non-pro server.
    
    
        The standard JAPC system property :code:`metafactory.service.config.lookups` should be set before
        :code:`ParameterFactory` is initialized.
    """
    def getConfig(self, string: str) -> typing.MutableSequence[java.util.Properties]:
        """
        
            Specified by:
                :code:`getConfig` in interface :code:`cern.japc.core.spi.factory.ServiceConfigLookup`
        
        
        """
        ...

class LsaNonproEnvironmentServiceConfigLookup(cern.japc.core.spi.factory.ServiceConfigLookup):
    """
    public class LsaNonproEnvironmentServiceConfigLookup extends java.lang.Object implements cern.japc.core.spi.factory.ServiceConfigLookup
    
        Should be used by both LSA server and clients to redirect japc operations to the a non-pro server.
    
    
        The standard JAPC system property :code:`metafactory.service.config.lookups` should be set before
        :code:`ParameterFactory` is initialized.
    """
    def __init__(self): ...
    def getConfig(self, string: str) -> typing.MutableSequence[java.util.Properties]:
        """
        
            Specified by:
                :code:`getConfig` in interface :code:`cern.japc.core.spi.factory.ServiceConfigLookup`
        
        
        """
        ...
    @staticmethod
    def setServerPropertyFileName(string: str) -> None: ...

class ClientNonProEnvironmentServiceConfigLookup(AbstractNonProEnvironmentServiceConfigLookup):
    """
    public class ClientNonProEnvironmentServiceConfigLookup extends :class:`~cern.lsa.client.spi.japc.AbstractNonProEnvironmentServiceConfigLookup`
    
        Should be used by LSA clients to redirect japc operations to the a non-pro server.
    """
    def __init__(self): ...

class ServerNonProEnvironmentServiceConfigLookup(AbstractNonProEnvironmentServiceConfigLookup):
    """
    public class ServerNonProEnvironmentServiceConfigLookup extends :class:`~cern.lsa.client.spi.japc.AbstractNonProEnvironmentServiceConfigLookup`
    
        Should be used by LSA servers to redirect japc operations to the a non-pro server.
    """
    def __init__(self): ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.spi.japc")``.

    AbstractNonProEnvironmentServiceConfigLookup: typing.Type[AbstractNonProEnvironmentServiceConfigLookup]
    ClientNonProEnvironmentServiceConfigLookup: typing.Type[ClientNonProEnvironmentServiceConfigLookup]
    LsaNonproEnvironmentServiceConfigLookup: typing.Type[LsaNonproEnvironmentServiceConfigLookup]
    ServerNonProEnvironmentServiceConfigLookup: typing.Type[ServerNonProEnvironmentServiceConfigLookup]
