import cern.lsa.domain.commons.logging
import java.lang
import java.lang.reflect
import jpype
import org.springframework.beans.factory
import org.springframework.remoting.rmi
import org.springframework.remoting.support
import typing



class LSAServiceAwareRmiProxyFactoryBean(org.springframework.remoting.rmi.RmiProxyFactoryBean, org.springframework.beans.factory.BeanNameAware):
    """
    public class LSAServiceAwareRmiProxyFactoryBean extends org.springframework.remoting.rmi.RmiProxyFactoryBean implements org.springframework.beans.factory.BeanNameAware
    """
    def __init__(self, string: str, int: int): ...
    def afterPropertiesSet(self) -> None:
        """
        
            Specified by:
                :code:`afterPropertiesSet` in interface :code:`org.springframework.beans.factory.InitializingBean`
        
            Overrides:
                :code:`afterPropertiesSet` in class :code:`org.springframework.remoting.rmi.RmiProxyFactoryBean`
        
        
        """
        ...
    def getObject(self) -> typing.Any:
        """
        
            Specified by:
                :code:`getObject` in interface :code:`org.springframework.beans.factory.FactoryBean<java.lang.Object>`
        
            Overrides:
                :code:`getObject` in class :code:`org.springframework.remoting.rmi.RmiProxyFactoryBean`
        
        
        """
        ...
    def setBeanName(self, string: str) -> None:
        """
        
            Specified by:
                :code:`setBeanName` in interface :code:`org.springframework.beans.factory.BeanNameAware`
        
        
        """
        ...
    def setInterfaceClassName(self, string: str) -> None: ...
    def setLogicalInterfaceName(self, string: str) -> None: ...
    class FilteringStackTraceInvocationHandler(java.lang.reflect.InvocationHandler):
        PROPERTY_STACKTRACE_FILTER_DISABLED: typing.ClassVar[str] = ...
        PROPERTY_STACKTRACE_FILTER_PATTERNS: typing.ClassVar[str] = ...
        def invoke(self, object: typing.Any, method: java.lang.reflect.Method, objectArray: typing.Union[typing.List[typing.Any], jpype.JArray]) -> typing.Any: ...

class LoggingAwareRemoteInvocationResult(org.springframework.remoting.support.RemoteInvocationResult):
    """
    public class LoggingAwareRemoteInvocationResult extends org.springframework.remoting.support.RemoteInvocationResult
    
        Invocation result which keeps array of :code:`LogMessage`s.
    
        Also see:
            :meth:`~serialized`
    """
    @typing.overload
    def __init__(self, object: typing.Any): ...
    @typing.overload
    def __init__(self, throwable: java.lang.Throwable): ...
    def getMiddleTierLogs(self) -> typing.MutableSequence[cern.lsa.domain.commons.logging.LogMessage]: ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.common.spi.remoting")``.

    LSAServiceAwareRmiProxyFactoryBean: typing.Type[LSAServiceAwareRmiProxyFactoryBean]
    LoggingAwareRemoteInvocationResult: typing.Type[LoggingAwareRemoteInvocationResult]
