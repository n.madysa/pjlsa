import cern.lsa.client.common.spi.japc
import cern.lsa.client.common.spi.remoting
import typing



class EnvironmentResolver:
    """
    public final class EnvironmentResolver extends java.lang.Object
    
        Helper to identify what environment the application is running i.e.: PRO, NEXT, DEV
    """
    @staticmethod
    def isDev() -> bool:
        """
            Indicates if the application runs with DEV server (in 3-tier mode) or with DEV database (in 2-tier mode).
        
        """
        ...
    @staticmethod
    def isNext() -> bool:
        """
            Indicates if the application runs with NEXT server (in 3-tier mode) or with NEXT database (in 2-tier mode).
        
        """
        ...
    @staticmethod
    def isPro() -> bool:
        """
            Indicates if the application runs with PRO server (in 3-tier mode) or with PRO database (in 2-tier mode).
        
        """
        ...
    @staticmethod
    def isTest() -> bool:
        """
            Indicates if the application runs with TEST database (in 2-tier mode).
        
        """
        ...
    @staticmethod
    def isTestbed() -> bool:
        """
            Indicates if the application runs with TESTBED server (in 3-tier mode) or with TESTBED database (in 2-tier mode).
        
        """
        ...
    @staticmethod
    def isTwoTier() -> bool: ...

class ServerEnvironmentResolver:
    """
    public final class ServerEnvironmentResolver extends java.lang.Object
    """
    @staticmethod
    def isDev() -> bool:
        """
            Indicates if the server runs in DEV environment i.e. on the DEV database.
        
        """
        ...
    @staticmethod
    def isNext() -> bool:
        """
            Indicates if the server runs in NEXT environment i.e. on the NEXT database.
        
        """
        ...
    @staticmethod
    def isPro() -> bool:
        """
            Indicates if the server runs in PRO environment i.e. on the PRO database.
        
        """
        ...
    @staticmethod
    def isTest() -> bool:
        """
            Indicates if the server runs in TEST environment i.e. on the TEST database.
        
        """
        ...
    @staticmethod
    def isTestbed() -> bool:
        """
            Indicates if the server runs in TESTBED environment i.e. on the TESTBED database.
        
        """
        ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.common.spi")``.

    EnvironmentResolver: typing.Type[EnvironmentResolver]
    ServerEnvironmentResolver: typing.Type[ServerEnvironmentResolver]
    japc: cern.lsa.client.common.spi.japc.__module_protocol__
    remoting: cern.lsa.client.common.spi.remoting.__module_protocol__
