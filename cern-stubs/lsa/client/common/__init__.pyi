import cern.accsoft.commons.domain
import cern.japc.core
import cern.japc.value
import cern.lsa.client.common.cache
import cern.lsa.client.common.japc
import cern.lsa.client.common.spi
import cern.lsa.client.common.test
import cern.lsa.domain
import cern.lsa.domain.commons
import cern.lsa.domain.devices
import cern.lsa.domain.devices.type
import cern.lsa.domain.exploitation
import cern.lsa.domain.exploitation.command
import cern.lsa.domain.generation
import cern.lsa.domain.optics
import cern.lsa.domain.settings
import cern.lsa.domain.settings.parameter.relation
import cern.lsa.domain.settings.parameter.type.relation
import cern.lsa.domain.settings.type
import cern.lsa.domain.trim.rules.makerule
import java.lang
import java.util
import typing



class ClientException(cern.lsa.domain.LsaException):
    """
    public class ClientException extends cern.lsa.domain.LsaException
    
        A general exception that is the root of all exceptions occuring in the business of the application.
    
        Also see:
            :meth:`~serialized`
    """
    @typing.overload
    def __init__(self, exception: java.lang.Exception): ...
    @typing.overload
    def __init__(self, string: str): ...
    @typing.overload
    def __init__(self, string: str, exception: java.lang.Exception): ...

class CommonAcceleratorService:
    """
    @Cacheable("clientCache") public interface CommonAcceleratorService
    
        Service for operations related to fetching and manipulating information about the accelerators (such as accelerator
        zones, particle transfers, ...).
    """
    def findAccelerator(self, string: str) -> cern.accsoft.commons.domain.Accelerator:
        """
            Returns accelerator with specified name.
        
            Parameters:
                acceleratorName (java.lang.String): 
            Returns:
                :code:`Accelerator` or :code:`null` if it can't be found
        
        
        """
        ...
    def findAccelerators(self) -> java.util.Set[cern.accsoft.commons.domain.Accelerator]:
        """
            Returns a set of known accelerators.
        
            In order to find a particular accelerator in the returned set, consider using
            :code:`Nameds.mapByName(java.util.Collection)`. Ex.:
        
            .. code-block: java
            
             Set<Accelerator> accelerators = acceleratorService.findAccelerators();
             Map<String, Accelerator> namesMap = Nameds.mapByName(accelerators);
             Accelerator lhc = namesMap.get("LHC");
             
        
            Alternatively use directly one of the predefined accelerators as defined in CernAccelerator enum.
        
            Returns:
                set of known accelerators
        
        
        """
        ...

class CommonArchiveReferenceService:
    """
    public interface CommonArchiveReferenceService
    
        A service that allows retrieval, creation, modification and deletion of setting archives and retrieval and updates of
        setting reference values.
    
        Also see:
            :code:`Archive`
    """
    def createArchive(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext, string: str, string2: str) -> cern.lsa.domain.settings.Archive:
        """
            Creates a new archive for all parameters in the given stand-alone context, with given name, description and version =
            1.0.
        
            The created archive will contain all the active (current) settings of the specified context.
        
            Parameters:
                standAloneContext (cern.lsa.domain.settings.StandAloneContext): the non null context for which the archive should be created
                name (java.lang.String): the non null name of the new archive
                description (java.lang.String): the non null description of version 1.0 of the archive
        
            Returns:
                created archive
        
            Raises:
                java.lang.IllegalArgumentException: in case there are no settings associated with given context or there is already an archive with specified name (for this
                    context)
        
        
        """
        ...
    def createArchiveVersion(self, archive: cern.lsa.domain.settings.Archive, string: str, *string2: str) -> cern.lsa.domain.settings.Archive:
        """
            Creates a new version of the specified archive.
        
            If the :code:`parameterNames` argument is empty (omitted or null), the created archive will contain all the active
            (current) settings of the associated stand-alone context. In such a case the new archive version will have the next
            major number e.g. 1.2 becomes 2.0.
        
            If parameter names are specified the created archive contains active settings of these parameters (unless these
            parameters have no settings). Settings of all other parameters (not listed) are copied from the latest version of the
            given archive. In this situation the new archive version will have the next minor number e.g. 1.2 becomes 1.3.
        
            Parameters:
                archive (cern.lsa.domain.settings.Archive): the non null archive for which a new version should be created
                description (java.lang.String): the non null description of the new archive version
                parameterNames (java.lang.String...): (optional) names of parameters whose active settings will be used to create a new archive version
        
            Returns:
                the :code:`Archive` object along with the new version
        
            Raises:
                cern.lsa.domain.settings.ParameterNotFoundException: if one or more of specified parameters is not defined in LSA
        
            Also see:
                :code:`Archive.getLatestVersion()`
        
        
        """
        ...
    def deleteArchive(self, archive: cern.lsa.domain.settings.Archive) -> None:
        """
            Deletes the specified archive with all its versions.
        
            Parameters:
                archive (cern.lsa.domain.settings.Archive): the archive to be deleted
        
        
        """
        ...
    def deleteArchiveVersion(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion) -> cern.lsa.domain.settings.Archive:
        """
            Deletes the specified archive version.
        
            Parameters:
                archiveVersion (cern.lsa.domain.settings.ArchiveVersion): the non null archive version to be deleted
        
            Returns:
                the updated archive, not containing the specified archive version
        
            Raises:
                java.lang.IllegalArgumentException: in case the specified version can't be deleted as it is the last one of this archive. In such case
                    :meth:`~cern.lsa.client.common.CommonArchiveReferenceService.deleteArchive` method should be used instead.
        
        
        """
        ...
    def findArchiveById(self, long: int) -> cern.lsa.domain.settings.Archive:
        """
            Returns archive with given ID.
        
            Parameters:
                archiveId (long): the identifier of the archive
        
            Returns:
                archive with specified ID or :code:`null` if not found
        
            Also see:
                :code:`IdentifiedEntity.getId()`
        
        
        """
        ...
    @typing.overload
    def findArchiveSettings(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion) -> cern.lsa.domain.settings.CompositeContextSettings:
        """
            Returns an :code:`CompositeContextSettings` object for the given archive version. The method returns archived settings
            for the given (non null and not empty) list of parameters.
        
            If some of specified parameters are not defined in LSA, a :code:`ParameterNotFoundException` is thrown.
        
            If some of specified parameters don't have settings in the considered archive, they are ignored and not returned in the
            :code:`CompositeContextSettings` object.
        
            Parameters:
                archiveVersion (cern.lsa.domain.settings.ArchiveVersion): the non null :code:`archiveVersion` of the considered archive
                parameterNames (java.util.Collection<java.lang.String> parameterNames): non null and not empty parameters' names to get settings for.
                    :meth:`~cern.lsa.client.common.CommonArchiveReferenceService.findArchiveSettings` to get
                    :code:`CompositeContextSettings` for all parameters
        
            Returns:
                :code:`CompositeContextSettings` for given version, containing settings for all or specified parameters
        
            Raises:
                cern.lsa.domain.settings.ParameterNotFoundException: if one or more of specified parameters is not defined in LSA
        
            Returns an :code:`CompositeContextSettings` object for the given archive version and all parameters associated with it.
        
            Parameters:
                archiveVersion (cern.lsa.domain.settings.ArchiveVersion): the non null :code:`archiveVersion` of the considered archive
        
            Returns:
                :code:`CompositeContextSettings` for given version, containing settings for all or specified parameters
        
            Raises:
                cern.lsa.domain.settings.ParameterNotFoundException: if one or more of specified parameters is not defined in LSA
        
        
        """
        ...
    @typing.overload
    def findArchiveSettings(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> cern.lsa.domain.settings.CompositeContextSettings: ...
    def findArchives(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> java.util.List[cern.lsa.domain.settings.Archive]:
        """
            Returns the list of archives for the given stand-alone context.
        
            Parameters:
                standAloneContext (cern.lsa.domain.settings.StandAloneContext): the non null :code:`standAloneContext`
        
            Returns:
                the non null list of archives. If there are no archives for given context - an empty list is returned
        
        
        """
        ...
    def findParametersWithoutSettings(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion, *string: str) -> java.util.Set[str]:
        """
            Returns a set of parameter names that don't have settings in the specified archive version.
        
            If parameter names are specified, the method checks only these parameters. Otherwise the method checks all parameters
            defined in LSA for the associated accelerator, taking into account particle transfers used in this context e.g. if a
            stand-alone cycle has beam processes only for PSRING and TT2 - there are clearly no settings for F61 - so parameters
            from this particle transfer won't be returned.
        
            Parameters:
                archiveVersion (cern.lsa.domain.settings.ArchiveVersion): the non null archive version to be checked
                parameterNames (java.lang.String...): list of parameters to be checked. If empty (omitted or null) - all used parameters are checked (see description above).
        
            Returns:
                non null set of parameters' names without settings in the archive version
        
            Raises:
                cern.lsa.domain.settings.ParameterNotFoundException: if one or more of specified parameters are not defined in LSA
        
        
        """
        ...
    @typing.overload
    def findReferenceValues(self, drivableContext: cern.lsa.domain.settings.DrivableContext) -> cern.lsa.domain.settings.CompositeContextSettings:
        """
            Returns an :code:`CompositeContextSettings` object with reference values for specified parameters and drivable context.
        
            Note that values for non-multiplexed parameters will be looked up in the associated non-multiplexed context.
        
            Parameters:
                drivableContext (cern.lsa.domain.settings.DrivableContext): the non null drivable context for which reference value should be returned
                parameterNames (java.util.Collection<java.lang.String> parameterNames): the non empty parameter name collection to get the associated reference values for
        
            Returns:
                :code:`CompositeContextSettings` object, containing settings for multiplexed and non-multiplexed parameters
        
            Returns a :code:`CompositeContextSettings` object with reference values for specified drivable context and all
            parameters associated with the particle transfers within this context
        
            Note that values for non-multiplexed parameters will be looked up in the associated non-multiplexed context.
        
            Parameters:
                drivableContext (cern.lsa.domain.settings.DrivableContext): the non null drivable context for which reference value should be returned
        
            Returns:
                :code:`CompositeContextSettings` object, containing settings for multiplexed and non-multiplexed parameters
        
        
        """
        ...
    @typing.overload
    def findReferenceValues(self, drivableContext: cern.lsa.domain.settings.DrivableContext, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> cern.lsa.domain.settings.CompositeContextSettings: ...
    def restoreArchive(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion, *string: str) -> cern.lsa.domain.settings.SettingsRestoreStatus: ...
    def restoreReferences(self, drivableContext: cern.lsa.domain.settings.DrivableContext, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> cern.lsa.domain.settings.SettingsRestoreStatus: ...
    def saveReferenceValues(self, drivableContext: cern.lsa.domain.settings.DrivableContext, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> java.util.Set[str]:
        """
            Saves reference values for specified parameters and drivable context.
        
            Note that non-multiplexed parameters will be saved using the associated non-multiplexed context.
        
            Parameters:
                drivableContext (cern.lsa.domain.settings.DrivableContext): the non null drivable context for which reference value should be saved
                parameterNames (java.util.Collection<java.lang.String> parameterNames): the non empty parameter name list for which reference values should be saved
        
            Returns:
                set of failed parameters (parameters were not found in LSA or there is no settings)
        
        
        """
        ...
    def updateArchiveName(self, archive: cern.lsa.domain.settings.Archive, string: str) -> cern.lsa.domain.settings.Archive:
        """
            Updates name of the specified archive.
        
            Parameters:
                archive (cern.lsa.domain.settings.Archive): the non null archive to be updated
                name (java.lang.String): the new (non null) name of the archive
        
            Returns:
                the updated (renamed) archive
        
            Raises:
                java.lang.IllegalArgumentException: in case there is already an archive with specified name (for this context)
        
            Also see:
                :code:`Named.getName()`
        
        
        """
        ...
    def updateArchiveVersionDescription(self, archiveVersion: cern.lsa.domain.settings.ArchiveVersion, string: str) -> cern.lsa.domain.settings.Archive:
        """
            Updates description of the specified archive version.
        
            Parameters:
                archiveVersion (cern.lsa.domain.settings.ArchiveVersion): the non null header of archive.
                description (java.lang.String): the non null description of new archive
        
            Returns:
                the updated archive containing the version with the new description
        
            Also see:
                :code:`ArchiveVersion.getDescription()`
        
        
        """
        ...

class CommonCacheService:
    """
    public interface CommonCacheService
    
        A service that allows to clear all caches on the server-side.
    """
    def clearAll(self) -> None:
        """
            Clears all server caches.
        
        """
        ...

class CommonContextService:
    """
    @Cacheable("clientCache") public interface CommonContextService
    
        Service for context-related information: beam processes, cycles, hypercycles, cycle to user mappings...
    """
    def findAcceleratorUser(self, acceleratorUsersRequest: cern.lsa.domain.settings.AcceleratorUsersRequest) -> cern.lsa.domain.settings.AcceleratorUser: ...
    def findAcceleratorUsers(self, acceleratorUsersRequest: cern.lsa.domain.settings.AcceleratorUsersRequest) -> java.util.Set[cern.lsa.domain.settings.AcceleratorUser]:
        """
            Returns all :code:`AcceleratorUser`s matching the criteria specified in the given request.
        
            Parameters:
                acceleratorUsersRequest (cern.lsa.domain.settings.AcceleratorUsersRequest): Request with criteria to be matched by the :code:`AcceleratorUser`s searched for.
        
            Returns:
                All :code:`AcceleratorUser`s matching criteria specified in the given request.
        
        
        """
        ...
    def findBeamProcessPurposes(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.type.BeamProcessPurpose]:
        """
            Returns all :code:`BeamProcessPurpose`s defined in the database.
        
            Parameters:
                accelerator (cern.accsoft.commons.domain.Accelerator): the accelerator for which to fetch :code:`BeamProcessPurpose`
        
            Returns:
                non-null set of :code:`BeamProcessPurpose`s
        
        
        """
        ...
    def findContextCategories(self) -> java.util.Set[cern.lsa.domain.settings.ContextCategory]:
        """
            Returns all context categories defined in the database.
        
            Returns:
                non-null set of categories
        
        
        """
        ...
    def findDefaultBeamProcessPurpose(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> cern.lsa.domain.settings.type.BeamProcessPurpose:
        """
            Returns default :code:`BeamProcessPurpose`.
        
            Parameters:
                accelerator (cern.accsoft.commons.domain.Accelerator): The :code:`Accelerator` for which to fetch the default :code:`BeamProcessPurpose`. Must not be null.
        
            Returns:
                default :code:`BeamProcessPurpose`
        
        
        """
        ...
    def findDefaultContextCategory(self) -> cern.lsa.domain.settings.ContextCategory:
        """
            Returns default context category.
        
            Returns:
                default context category
        
        
        """
        ...
    def findDrivableContextByAcceleratorUser(self, acceleratorUser: cern.lsa.domain.settings.AcceleratorUser) -> cern.lsa.domain.settings.DrivableContext: ...
    def findDrivableContextByUser(self, string: str) -> cern.lsa.domain.settings.DrivableContext: ...
    def findResidentContexts(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.StandAloneContext]: ...
    def findResidentNonMultiplexedContext(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> cern.lsa.domain.settings.StandAloneContext:
        """
            Retrieves the non-multiplexed context that is resident for the given accelerator.
        
            Parameters:
                accelerator (cern.accsoft.commons.domain.Accelerator): the accelerator
        
            Returns:
                resident non-multiplexed context for given accelerator or :code:`null` if not defined
        
            Also see:
                :code:`)`
        
        
        """
        ...
    def findStandAloneBeamProcess(self, string: str) -> cern.lsa.domain.settings.StandAloneBeamProcess:
        """
            Returns the stand-alone beam process with the given name, or :code:`null` if it doesn't exist.
        
            Parameters:
                beamProcessName (java.lang.String): the name of the beam process
        
            Returns:
                the beam process with given name or :code:`null`
        
        
        """
        ...
    @typing.overload
    def findStandAloneBeamProcesses(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.StandAloneBeamProcess]:
        """
            Returns all the :code:`StandAloneBeamProcess` corresponding to the request :code:`StandAloneBeamProcessesRequest`.
        
            Parameters:
                standAloneBeamProcessesRequest (cern.lsa.domain.settings.StandAloneBeamProcessesRequest): the request object
        
            Returns:
                non-null set of :code:`StandAloneBeamProcess` matching request
        
        @Deprecated java.util.Set<cern.lsa.domain.settings.StandAloneBeamProcess> findStandAloneBeamProcesses (cern.accsoft.commons.domain.Accelerator accelerator)
        
            Deprecated.
            use :code:`StandAloneBeamProcessesRequest.byAccelerator(Accelerator)` instead
            Returns the stand-alone beam processes for the given accelerator.
        
            Parameters:
                accelerator (cern.accsoft.commons.domain.Accelerator): the accelerator
        
            Returns:
                non-null set of beam processes for given accelerator
        
        
        """
        ...
    @typing.overload
    def findStandAloneBeamProcesses(self, standAloneBeamProcessesRequest: cern.lsa.domain.settings.StandAloneBeamProcessesRequest) -> java.util.Set[cern.lsa.domain.settings.StandAloneBeamProcess]: ...
    def findStandAloneContextByAcceleratorUser(self, acceleratorUser: cern.lsa.domain.settings.AcceleratorUser) -> cern.lsa.domain.settings.StandAloneContext: ...
    def findStandAloneContextByUser(self, string: str) -> cern.lsa.domain.settings.StandAloneContext: ...
    def findStandAloneContexts(self, standAloneContextsRequest: cern.lsa.domain.settings.StandAloneContextsRequest) -> java.util.Set[cern.lsa.domain.settings.StandAloneContext]:
        """
            Returns all the :code:`StandAloneContext` corresponding to the request :code:`StandAloneContextsRequest`.
        
            Parameters:
                standAloneContextsRequest (cern.lsa.domain.settings.StandAloneContextsRequest): the request object
        
            Returns:
                non-null set of :code:`StandAloneContext` matching request
        
        
        """
        ...
    def findStandAloneCycle(self, string: str) -> cern.lsa.domain.settings.StandAloneCycle:
        """
            Returns the stand-alone cycle with the given name, or :code:`null` if it doesn't exist.
        
            Parameters:
                cycleName (java.lang.String): the name of the cycle
        
            Returns:
                the cycle of given name or :code:`null`
        
        
        """
        ...
    @typing.overload
    def findStandAloneCycles(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.StandAloneCycle]:
        """
            Returns all the :code:`StandAloneBeamProcess` corresponding to the request :code:`StandAloneCyclesRequest`.
        
            Parameters:
                standAloneCyclesRequest (cern.lsa.domain.settings.StandAloneCyclesRequest): the request object
        
            Returns:
                non-null set of :code:`StandAloneCycle` matching request
        
        @Deprecated java.util.Set<cern.lsa.domain.settings.StandAloneCycle> findStandAloneCycles (cern.accsoft.commons.domain.Accelerator accelerator)
        
            Deprecated.
            use :code:`StandAloneCyclesRequest.byAccelerator(Accelerator)` instead
            Returns the stand-alone cycles for a given accelerator.
        
            Parameters:
                accelerator (cern.accsoft.commons.domain.Accelerator): the accelerator
        
            Returns:
                non-null set of cycles for given accelerator
        
        
        """
        ...
    @typing.overload
    def findStandAloneCycles(self, standAloneCyclesRequest: cern.lsa.domain.settings.StandAloneCyclesRequest) -> java.util.Set[cern.lsa.domain.settings.StandAloneCycle]: ...
    def findUserContextMappingHistory(self, accelerator: cern.accsoft.commons.domain.Accelerator, contextFamily: cern.lsa.domain.settings.ContextFamily, long: int, long2: int) -> java.util.List[cern.lsa.domain.settings.UserContextMapping]:
        """
            Retrieves the history of user to context mappings for an accelerator from specified time period.
        
            Parameters:
                accelerator (cern.accsoft.commons.domain.Accelerator): the accelerator
                fromTimestamp (cern.lsa.domain.settings.ContextFamily): begin of the desirable period of time
                toTimestamp (long): end of the desirable period of time
        
            Returns:
                non-null list of all mapping changes
        
        
        """
        ...
    def findUsers(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[str]: ...
    def saveContextToUserMapping(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.StandAloneContext], typing.Sequence[cern.lsa.domain.settings.StandAloneContext]]) -> None: ...
    def updateContext(self, context: cern.lsa.domain.settings.Context) -> None: ...

class CommonDeviceService:
    """
    @Cacheable("clientCache") public interface CommonDeviceService
    
        Service to work with devices, device groups, etc.
    """
    def deleteDeviceGroups(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.devices.DeviceGroup], typing.Sequence[cern.lsa.domain.devices.DeviceGroup]]) -> None: ...
    def findActualDevicesByLogicalHardwareName(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> java.util.Map[str, java.util.Set[cern.lsa.domain.devices.Device]]:
        """
            Returns the actual :code:`Device`s categorized by :code:`LogicalHardware` names, to which they correspond. If there are
            no :code:`Device`s for one of the given :code:`LogicalHardware`, an empty set is present in the mapping.
        
            Parameters:
                logicalHardwareNames (java.util.Collection<java.lang.String> logicalHardwareNames): names of the :code:`LogicalHardware`s for which the actual :code:`Device`s should be found
        
            Returns:
                mapping: logical hardware name to set of actual devices
        
        
        """
        ...
    def findCalibration(self, calibrationsRequest: cern.lsa.domain.devices.CalibrationsRequest) -> cern.lsa.domain.optics.Calibration: ...
    def findCalibrations(self, calibrationsRequest: cern.lsa.domain.devices.CalibrationsRequest) -> java.util.Set[cern.lsa.domain.optics.Calibration]: ...
    def findDevice(self, string: str) -> cern.lsa.domain.devices.Device:
        """
            Finds device by its name.
        
            Parameters:
                deviceName (java.lang.String): 
            Returns:
                the device or :code:`null` if it could not be found
        
        
        """
        ...
    def findDeviceGroupTypes(self) -> java.util.Set[cern.lsa.domain.devices.DeviceGroupType]:
        """
            Returns a set of all device group types.
        
            Returns:
                a set of all device group types
        
        
        """
        ...
    def findDeviceGroups(self, deviceGroupsRequest: cern.lsa.domain.devices.DeviceGroupsRequest) -> java.util.Set[cern.lsa.domain.devices.DeviceGroup]:
        """
            Returns all device groups matching criteria specified in the given request. The request should be created using
            :code:`DeviceGroupsRequestBuilder`.
        
            Parameters:
                request (cern.lsa.domain.devices.DeviceGroupsRequest): request with criteria to be matched by searched device groups
        
            Returns:
                all device groups matching criteria specified in the given request
        
            Also see:
                :code:`DeviceGroupsRequestBuilder`
        
        
        """
        ...
    def findDeviceType(self, string: str) -> cern.lsa.domain.devices.DeviceType:
        """
            Finds device type by its name.
        
            Parameters:
                deviceTypeName (java.lang.String): 
            Returns:
                device type with the specified name or :code:`null` if it can't be found
        
        
        """
        ...
    def findDeviceTypes(self, deviceTypesRequest: cern.lsa.domain.devices.DeviceTypesRequest) -> java.util.Set[cern.lsa.domain.devices.DeviceType]:
        """
            Returns all device types matching criteria specified in the given request. The request should be created using
            :code:`DeviceTypesRequestBuilder`.
        
            Parameters:
                request (cern.lsa.domain.devices.DeviceTypesRequest): request with criteria to be matched by searched device types
        
            Returns:
                all device types matching criteria specified in the given request
        
            Also see:
                :code:`DeviceTypesRequestBuilder`
        
        
        """
        ...
    def findDevices(self, devicesRequest: cern.lsa.domain.devices.DevicesRequest) -> java.util.Set[cern.lsa.domain.devices.Device]:
        """
            Returns all devices matching criteria specified in the given request. The request should be created using
            :code:`DevicesRequestBuilder`.
        
            Parameters:
                request (cern.lsa.domain.devices.DevicesRequest): request with criteria to be matched by searched devices
        
            Returns:
                all devices matching criteria specified in the given request
        
            Also see:
                :code:`DevicesRequestBuilder`
        
        
        """
        ...
    def findDevicesByGroups(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.devices.DeviceGroup], typing.Sequence[cern.lsa.domain.devices.DeviceGroup]]) -> java.util.Map[cern.lsa.domain.devices.DeviceGroup, java.util.List[cern.lsa.domain.devices.Device]]:
        """
            Returns map with devices belonging to specified groups. Devices for each group are sorted by their position in the
            group. If a given group doesn't contain any devices - the corresponding list is empty.
        
            Returns:
                non-null map with devices by groups
        
        
        """
        ...
    def findLogicalHardware(self, devicesRequest: cern.lsa.domain.devices.DevicesRequest) -> java.util.Set[cern.lsa.domain.optics.LogicalHardware]:
        """
            Finds the LogicalHardware objects according to the given request. The request should be created using
            :code:`DevicesRequestBuilder`.
        
            Parameters:
                request (cern.lsa.domain.devices.DevicesRequest): request with criteria to be matched by searched devices
        
            Returns:
                set of :code:`LogicalHardware` objects of devices matching criteria specified in the given request
        
            Also see:
                :code:`DevicesRequestBuilder`
        
        
        """
        ...
    def findLogicalHardwaresByActualDeviceNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> java.util.Map[str, java.util.Set[cern.lsa.domain.optics.LogicalHardware]]:
        """
            Returns the :code:`LogicalHardware` categorized by actual :code:`Device`s names, to which they correspond. If there are
            no :code:`LogicalHardware`s for one of the given actual :code:`Device`, an empty set is present in the mapping.
        
            Parameters:
                actualDeviceNames (java.util.Collection<java.lang.String> actualDeviceNames): names of the :code:`Device`s for which the :code:`LogicalHardware`s should be found
        
            Returns:
                mapping: actual device name to set of logical hardware
        
        
        """
        ...
    def findLogicalNamesByMadStrengthNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> java.util.Map[str, str]:
        """
            Returns a map which associates to some devices logical names the length of these devices. If among specified names, some
            logical devices are not found - the resulting map will not contain corresponding entries.
        
            The length attribute is looked up in the :code:`LOGICAL_HARDWARE_INFO` table.
        
            Parameters:
                strengthNames (java.util.Collection<java.lang.String> strengthNames): 
            Returns:
                a non-null map where keys are logical device names and values are strengths
        
        
        """
        ...
    def findMadStrengthNamesByLogicalNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> java.util.Map[str, str]:
        """
            Returns a map containing mapping from logical hardware names to MAD strength names. If among specified names, some
            logical devices are not found - the resulting map will not contain corresponding entries.
        
            The MAD strength names are looked up in the :code:`LOGICAL_HARDWARE_INFO` table.
        
            Parameters:
                logicalNamesArray (java.util.Collection<java.lang.String> logicalNamesArray): considered names of the logical devices
        
            Returns:
                a non-null map where the keys are logical device names and values are MAD strength names
        
        
        """
        ...
    def findPowerConverterInfo(self, string: str) -> cern.lsa.domain.optics.PowerConverterInfo:
        """
            Finds the PowerConverter object identified by the given name.
        
            Parameters:
                powerConverterName (java.lang.String): the name of the power converter to retrieve.
        
            Returns:
                the :code:`PowerConverterInfo`
        
        
        """
        ...
    def findPropertyFields(self, propertyFieldsRequest: cern.lsa.domain.devices.type.PropertyFieldsRequest) -> java.util.Set[cern.lsa.domain.devices.type.PropertyField]: ...
    def findPropertyVersions(self, propertyVersionsRequest: cern.lsa.domain.devices.type.PropertyVersionsRequest) -> java.util.SortedMap[cern.lsa.domain.devices.DeviceTypeVersion, java.util.Set[cern.lsa.domain.devices.type.PropertyVersion]]:
        """
            Returns all properties matching criteria specified in the given request. The request should be created using
            :code:`PropertyVersionsRequestBuilder`. If device type names :code:`PropertyVersionsRequest.getDeviceTypeNames()` are
            specified, then only properties for the latest version of each device type :code:`DeviceType.getVersions()` are
            returned. To get properties for older version provide :code:`PropertyVersionsRequest.getDeviceTypeVersions()` directly.
        
            Parameters:
                request (cern.lsa.domain.devices.type.PropertyVersionsRequest): request with criteria to be matched by searched properties
        
            Returns:
                all properties matching criteria specified in the given request
        
            Also see:
                :code:`PropertyVersionsRequestBuilder`
        
        
        """
        ...
    def saveDeviceGroup(self, deviceGroup: cern.lsa.domain.devices.DeviceGroup) -> None: ...
    def saveDeviceGroupDevices(self, deviceGroup: cern.lsa.domain.devices.DeviceGroup, collection: typing.Union[java.util.Collection[cern.lsa.domain.devices.Device], typing.Sequence[cern.lsa.domain.devices.Device]]) -> None: ...
    def saveLogicalHardware(self, logicalHardware: cern.lsa.domain.optics.LogicalHardware) -> None: ...
    def setActiveCalibration(self, string: str, string2: str) -> None: ...

class CommonExploitationService:
    """
    public interface CommonExploitationService
    
        Service to drive the hardware for which settings are stored in the setting management system.
    """
    def drive(self, driveRequest: cern.lsa.domain.exploitation.DriveRequest) -> cern.lsa.domain.exploitation.DriveResult:
        """
            Drives settings to the hardware. The request argument must be created using :code:`DriveRequestBuilder`. See JavaDoc of
            :code:`this class` for details of how to create a valid :code:`DriveRequest`.
        
        
        
        
            As of the refactoring of 2014, this method does not throw :code:`DriveException` anymore. To find out if there were
            errors during drive, check :code:`DriveResult.containsErrors()`.
        
            Parameters:
                request (cern.lsa.domain.exploitation.DriveRequest): Request containing all necessary information for the drive
        
            Returns:
                Status of the drive. If the drive is not allowed (i.e.: non-PRO database is used), the :code:`DriveResult` will contain
                an exception with a message that explains this error for every :code:`Parameter`.
        
        
        """
        ...
    def executeHwCommand(self, hwCommandExecutionRequest: cern.lsa.domain.exploitation.command.HwCommandExecutionRequest) -> cern.lsa.domain.exploitation.command.HwCommandExecutionResponse:
        """
            Executes the command identified in the request with the parameter value given along the request.
        
            Parameters:
                request (cern.lsa.domain.exploitation.command.HwCommandExecutionRequest): the request identifying the command to execute and providing the value of the command parameters.
        
            Returns:
                a response providing the result of the execution of the command.
        
        
        """
        ...
    def findHwCommands(self, hwCommandsRequest: cern.lsa.domain.exploitation.command.HwCommandsRequest) -> java.util.Set[cern.lsa.domain.exploitation.command.HwCommand]: ...
    def performSettingsCheck(self, settingsOnlineCheckRequest: cern.lsa.domain.exploitation.SettingsOnlineCheckRequest) -> java.util.List[cern.japc.core.FailSafeParameterValue]: ...
    def readHardwareValues(self, readHardwareRequest: cern.lsa.domain.exploitation.ReadHardwareRequest) -> java.util.Map[cern.lsa.domain.settings.Parameter, cern.lsa.domain.exploitation.FailSafeImmutableValue]:
        """
            Reads parameter values from the hardware for context and parameters specified in the request.
        
            Parameters:
                request (cern.lsa.domain.exploitation.ReadHardwareRequest): describes what parameters and for what context should be read from the hardware
        
            Returns:
                map with settings loaded from the hardware
        
        
        """
        ...

class CommonGenerationService:
    """
    public interface CommonGenerationService
    
        Service for generation: cycle and beamprocess types creation and scheduling of beamprocess types.
    """
    def cloneStandAloneContext(self, standAloneContextCloneRequest: cern.lsa.domain.settings.StandAloneContextCloneRequest) -> cern.lsa.domain.settings.StandAloneContext: ...
    def createStandAloneContext(self, standAloneContextCreationRequest: cern.lsa.domain.settings.StandAloneContextCreationRequest) -> cern.lsa.domain.settings.StandAloneContext: ...
    def deleteContextType(self, contextType: cern.lsa.domain.settings.type.ContextType) -> None: ...
    def deleteStandAloneContext(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> None: ...
    def deleteValueGeneratorDefinitions(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.generation.ValueGeneratorDefinition], typing.Sequence[cern.lsa.domain.generation.ValueGeneratorDefinition]]) -> None: ...
    def findAllAvailableValueGenerators(self) -> java.util.Set[cern.lsa.domain.generation.ValueGeneratorInfo]: ...
    def findAllParticleTypes(self) -> java.util.Set[cern.accsoft.commons.domain.ParticleType]:
        """
            Returns all available particle types.
        
            Returns:
                all available particle types.
        
        
        """
        ...
    def findBeamProcessType(self, string: str) -> cern.lsa.domain.settings.type.BeamProcessType: ...
    def findBeamProcessTypeAttributeDefinitions(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.type.BeamProcessTypeSegmentAttributeDefinition]: ...
    def findBeamProcessTypeOpticTable(self, string: str) -> cern.lsa.domain.optics.OpticsTable: ...
    def findBeamProcessTypes(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.type.BeamProcessType]: ...
    def findContextNamesByType(self, contextType: cern.lsa.domain.settings.type.ContextType) -> java.util.Set[str]: ...
    def findCycleType(self, string: str) -> cern.lsa.domain.settings.type.CycleType: ...
    def findCycleTypeAttributeDefinitions(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.commons.AttributeDefinition]: ...
    def findCycleTypes(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.type.CycleType]: ...
    def findIncorporationRanges(self, string: str) -> java.util.List[cern.lsa.domain.settings.type.IncorporationRange]: ...
    def findIncorporationRules(self) -> java.util.List[cern.lsa.domain.settings.type.IncorporationRuleDescriptor]: ...
    def findStandAloneContexAttributeDefinitions(self, contextFamily: cern.lsa.domain.settings.ContextFamily, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.commons.AttributeDefinition]:
        """
            Returns all :code:`AttributeDefinition`s, which can be bound to a context of given category (see :code:`ContextFamily`)
            for the specified accelerator
        
            Parameters:
                contextFamily (cern.lsa.domain.settings.ContextFamily): specifies context family we are interested in
                accelerator (cern.accsoft.commons.domain.Accelerator): the accelerator for which the contexts are defined
        
            Returns:
                a set of :code:`AttributeDefinition`s which can be specified for a context with respect to given accelerator
        
        
        """
        ...
    def findValueGeneratorConfigInfo(self, parameter: cern.lsa.domain.settings.Parameter) -> cern.lsa.domain.generation.ValueGeneratorConfigInfo: ...
    def findValueGeneratorDefinitions(self, valueGeneratorDefinitionsRequest: cern.lsa.domain.generation.ValueGeneratorDefinitionsRequest) -> java.util.Set[cern.lsa.domain.generation.ValueGeneratorDefinition]: ...
    def generateActualBeamProcess(self, beamProcess: cern.lsa.domain.settings.BeamProcess, int: int) -> cern.lsa.domain.settings.StandAloneBeamProcess: ...
    def generateSettings(self, settingsGenerationRequest: cern.lsa.domain.settings.SettingsGenerationRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    def regenerateActualBeamProcess(self, standAloneBeamProcess: cern.lsa.domain.settings.StandAloneBeamProcess, string: str) -> None:
        """
            Regenerates settings of the given actual beam process.
        
            Parameters:
                actualBeamProcess (cern.lsa.domain.settings.StandAloneBeamProcess): the actual beam process to regenerate
                trimDescription (java.lang.String): optional trim comment to be used for the trim entry that will be created. If not given, the method will use
                    '(Re)Generated from [sourceBeamProcessName]'
        
        
        """
        ...
    def saveBeamProcessType(self, beamProcessType: cern.lsa.domain.settings.type.BeamProcessType) -> None: ...
    def saveCycleType(self, cycleType: cern.lsa.domain.settings.type.CycleType) -> None: ...
    def saveIncorporationRanges(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.type.IncorporationRange], typing.Sequence[cern.lsa.domain.settings.type.IncorporationRange]], string: str) -> None: ...
    def saveOpticTable(self, opticsTable: cern.lsa.domain.optics.OpticsTable) -> None:
        """
            save the opticsTable items in the database
        
            Parameters:
                opticsTable (cern.lsa.domain.optics.OpticsTable): 
        
        """
        ...
    def saveValueGeneratorDefinitions(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.generation.ValueGeneratorDefinition], typing.Sequence[cern.lsa.domain.generation.ValueGeneratorDefinition]]) -> None: ...

class CommonJapcService:
    """
    public interface CommonJapcService
    
        Bridge between JAPC and LSA parameters.
    """
    def getValue(self, string: str, selector: cern.japc.core.Selector) -> cern.japc.core.AcquiredParameterValue: ...
    def getValues(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]], selector: cern.japc.core.Selector) -> java.util.List[cern.japc.core.FailSafeParameterValue]: ...
    def setValue(self, string: str, selector: cern.japc.core.Selector, parameterValue: cern.japc.value.ParameterValue) -> None: ...

class CommonKnobService:
    """
    public interface CommonKnobService
    
        Service for knob related operations.
    """
    def deactivateKnob(self, string: str) -> None:
        """
            Deactivates a knob, i.e. it will be associated with the group "NOT_USED". The parameter itself and all settings continue
            to exist.
        
            Parameters:
                knobName (java.lang.String): the name of knob to deactivate
        
        
        """
        ...
    def deleteKnob(self, string: str) -> None:
        """
            Deletes a knob. It removes all associated values (that is, all related entries in the *knobs* table), the corresponding
            parameter and parameter relations to its components, and all associated settings.
        
            Note that it does not delete the parameters that hold the related knob components.
        
            Parameters:
                knobName (java.lang.String): name of the knob that is to be deleted
        
            Raises:
                java.lang.IllegalArgumentException: if the knob is not found
                java.lang.IllegalArgumentException: if the knob is protected or does not exist
        
        
        """
        ...
    def findKnob(self, string: str) -> cern.lsa.domain.settings.Knob:
        """
            Finds knob by its name.
        
            Parameters:
                knobName (java.lang.String): 
            Returns:
                the knob or :code:`null` if it could not be found
        
        
        """
        ...
    def findKnobs(self, parametersRequest: cern.lsa.domain.settings.ParametersRequest) -> java.util.Set[cern.lsa.domain.settings.Knob]:
        """
            Returns all knobs for parameters found by :code:`request`. If for parameter found by request a knob doesn't exists then
            it won't be included in returned set.
        
            Parameters:
                request (cern.lsa.domain.settings.ParametersRequest): request specifying for which parameters knobs should be returned
        
            Returns:
                set of the requested knobs
        
        
        """
        ...
    def saveKnob(self, knob: cern.lsa.domain.settings.Knob) -> cern.lsa.domain.settings.Knob:
        """
            Saves the knob structure and values. This method can be used for both new knob creation and existing knob modification.
        
            Parameters:
                knob (cern.lsa.domain.settings.Knob): 
            Returns:
                the saved knob (to be used after new knob creation)
        
        
        """
        ...

class CommonOpticService:
    """
    @Cacheable("clientCache") public interface CommonOpticService
    """
    def deleteMeasuredTwiss(self, *measuredTwiss: cern.lsa.domain.optics.MeasuredTwiss) -> int:
        """
            Deletes twiss measurements from the permanent storage.
        
            It is suggested to use the methods in the twiss finder, then filter the measurements that are to be deleted (with the
            appropriate helper methods, such as those in the :code:`TwissHelper`), and only then pass them as an array to this
            method for them to be deleted. That is, the selection of the twiss measurements to be deleted must be done before
            calling this method. Find some examples in the unit tests.
        
            *Javadoc copied from :code:`TwissPersister#deleteMeasuredTwiss(MeasuredTwiss...)`.*
        
            Parameters:
                measuredTwiss (cern.lsa.domain.optics.MeasuredTwiss...): one or several measurements for a twiss at a given energy
        
            Returns:
                number of twiss measurements that were deleted
        
        
        """
        ...
    def findElement(self, string: str) -> cern.lsa.domain.optics.Element:
        """
            Returns the element with the given name, or :code:`null` if it doesn't exist.
        
            Parameters:
                elementName (java.lang.String): 
            Returns:
                element with specified name or :code:`null` if it can't be found
        
        
        """
        ...
    def findElements(self, elementsRequest: cern.lsa.domain.optics.ElementsRequest) -> java.util.Set[cern.lsa.domain.optics.Element]:
        """
            Returns the elements satisfying the search criteria.
        
            Parameters:
                request (cern.lsa.domain.optics.ElementsRequest): 
            Returns:
                elements satisfying the request
        
        
        """
        ...
    @typing.overload
    def findMeasuredTwiss(self, twiss: cern.lsa.domain.optics.Twiss) -> java.util.List[cern.lsa.domain.optics.MeasuredTwiss]:
        """
            Finds all measurements performed for a twiss.
        
            *Javadoc copied from :code:`TwissFinder#findMeasuredTwiss(Twiss)`.*
        
            Parameters:
                twiss (cern.lsa.domain.optics.Twiss): a twiss
        
            Returns:
                a list that contains all the measurements performed for this twiss, sorted by energy and then by timstamp
        
            Also see:
                :code:`TwissHelper`
        
            Finds all measurements performed for an optic.
        
            *Javadoc copied from :code:`TwissFinder#findMeasuredTwiss(String)`.*
        
            Parameters:
                opticName (java.lang.String): name of an optic
        
            Returns:
                a list that contains all the measurements performed for this optic, sorted by energy and then by timestamp
        
            Also see:
                :code:`TwissHelper`
        
        
        """
        ...
    @typing.overload
    def findMeasuredTwiss(self, string: str) -> java.util.List[cern.lsa.domain.optics.MeasuredTwiss]: ...
    def findOpticById(self, long: int) -> cern.lsa.domain.optics.Optic:
        """
            Returns the optic with the given ID, or :code:`null` if it doesn't exist.
        
            Parameters:
                opticId (long): the ID of the optic
        
            Returns:
                optic of the given ID or :code:`null`
        
        
        """
        ...
    def findOpticByName(self, string: str) -> cern.lsa.domain.optics.Optic:
        """
            Returns the optic with the given name, or :code:`null` if it doesn't exist.
        
            Parameters:
                opticName (java.lang.String): the name of the optic
        
            Returns:
                optic of the given name or :code:`null`
        
        
        """
        ...
    def findOpticInBeamProcessType(self, string: str, int: int) -> cern.lsa.domain.optics.Optic:
        """
            Finds the optic for a given beam process type name and a given point in time.
        
            Parameters:
                beamProcessTypeName (java.lang.String): name of the beam process type
                time (int): time in the range of beam process type
        
            Returns:
                optic configured for given beam process at specified time
        
        
        """
        ...
    def findOpticNames(self, opticsRequest: cern.lsa.domain.optics.OpticsRequest) -> java.util.Set[str]:
        """
            Returns the names of the optics satisfying the search criteria.
        
            Parameters:
                request (cern.lsa.domain.optics.OpticsRequest): search criteria
        
            Returns:
                the names of the optics satisfying the search criteria
        
        
        """
        ...
    def findOptics(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> java.util.Set[cern.lsa.domain.optics.Optic]:
        """
            Returns the optics with the requested names.
        
            Parameters:
                opticNames (java.util.Collection<java.lang.String> opticNames): optic names
        
            Returns:
                the optics with the requested names
        
        
        """
        ...
    def findOpticsTables(self, opticsTablesRequest: cern.lsa.domain.optics.OpticsTablesRequest) -> java.util.Map[str, cern.lsa.domain.optics.OpticsTable]:
        """
            Returns a map with beam process type names pointing to corresponding :code:`OpticsTable`s fulfilling the request
        
            Parameters:
                request (cern.lsa.domain.optics.OpticsTablesRequest): object with one condition
        
            Returns:
                map with beam process type names pointing to corresponding :code:`OpticsTable`s fulfilling the request
        
        
        """
        ...
    def findTwisses(self, twissesRequest: cern.lsa.domain.optics.TwissesRequest) -> java.util.Set[cern.lsa.domain.optics.Twiss]: ...
    def insertMeasuredTwiss(self, *measuredTwiss: cern.lsa.domain.optics.MeasuredTwiss) -> None:
        """
            Inserts a new twiss measurement into the permanent storage.
        
            *Javadoc copied from :code:`TwissPersister#insertMeasuredTwiss(MeasuredTwiss...)`.*
        
            Parameters:
                measuredTwiss (cern.lsa.domain.optics.MeasuredTwiss...): measurements for a twiss at a given energy
        
        
        """
        ...
    def renameOptic(self, string: str, string2: str) -> None: ...
    def saveElements(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.optics.Element], typing.Sequence[cern.lsa.domain.optics.Element]]) -> None:
        """
            insert a list of elements in the persistent storage
        
            Parameters:
                elements (java.util.Collection<cern.lsa.domain.optics.Element> elements): 
        
        """
        ...
    def setElementsObsolete(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.optics.Element], typing.Sequence[cern.lsa.domain.optics.Element]]) -> None:
        """
            Mark the elements of the list as obsolete in the database
        
            Parameters:
                elements (java.util.Collection<cern.lsa.domain.optics.Element> elements): the elements to be marked as obsolete.
        
        
        """
        ...
    def updateElementName(self, string: str, string2: str) -> None:
        """
            update the name of an existing element with a new name
        
            Parameters:
                actualName (java.lang.String):         newName (java.lang.String): 
        
        """
        ...
    def updateElements(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.optics.Element], typing.Sequence[cern.lsa.domain.optics.Element]]) -> None:
        """
            update an existing element with the attributs value of the given object Do nothing if the element doesn't exist already
        
        """
        ...

class CommonParameterService:
    """
    @Cacheable("clientCache") public interface CommonParameterService
    
        A class implementing this interface provides methods to retrieve parameters and their associated objects.
    
        Caches for this service should be reviewed after methods for knobs in the OpticService will be implemented properly
    """
    def addParametersToParameterGroup(self, parameterGroup: cern.lsa.domain.settings.ParameterGroup, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> None: ...
    def deleteCriticalProperty(self, propertyVersion: cern.lsa.domain.devices.type.PropertyVersion, device: cern.lsa.domain.devices.Device) -> None: ...
    def deleteParameterGroup(self, long: int) -> None: ...
    def deleteParameterRelations(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.parameter.relation.ParameterRelation], typing.Sequence[cern.lsa.domain.settings.parameter.relation.ParameterRelation]]) -> None: ...
    def deleteParameterTypeRelations(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelation], typing.Sequence[cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelation]]) -> None: ...
    def deleteParameterTypes(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> None: ...
    def deleteParameters(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> None: ...
    def findAllAvailableMakerules(self) -> java.util.Set[cern.lsa.domain.trim.rules.makerule.MakeRuleInfo]:
        """
            get all the makerules that are available to LSA
        
        """
        ...
    def findAllHierarchies(self) -> java.util.List[str]:
        """
            Finds names of all hierarchies which are mentioned as default hierarchy for some parameters or for which there exist
            some relations. Because there is no dedicated table for storing information about types of hierarchies (names, etc.),
            names of hierarchies are extracted from this two mentioned sources.
        
        """
        ...
    def findAllParameterTypes(self) -> java.util.Set[cern.lsa.domain.settings.ParameterType]:
        """
            Return all the parameter types.
        
            Returns:
                all the parameter types objects in an unmodifiable set
        
        
        """
        ...
    def findCommonHierarchyNames(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.Parameter], typing.Sequence[cern.lsa.domain.settings.Parameter]]) -> java.util.Set[str]:
        """
            Return the names of all the hierarchy names, defined for the parameter relationships, which are common to all the input
            parameters. In other words, it returns the intersection of the hierarchy names of the input parameters
        
            Parameters:
                parameters (java.util.Collection<cern.lsa.domain.settings.Parameter> parameters): parameters whose common hierarchies have to be retrieved.
        
            Returns:
                the distinct names of all the common hierarchy names defined for the parameter relationships.
        
        
        """
        ...
    def findHierarchyNames(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.Parameter], typing.Sequence[cern.lsa.domain.settings.Parameter]]) -> java.util.Set[str]:
        """
            Return the names of all the hierarchy names defined for the parameter relationships.
        
            Parameters:
                parameters (java.util.Collection<cern.lsa.domain.settings.Parameter> parameters): parameter whose hierarchies to retrieve.
        
            Returns:
                the (distinct) names of all the hierarchy names defined for the parameter relationships.
        
        
        """
        ...
    def findMakeRuleForParameterRelation(self, parameterRelation: cern.lsa.domain.settings.parameter.relation.ParameterRelation) -> cern.lsa.domain.trim.rules.makerule.MakeRuleConfigInfo:
        """
            Finds the information about the makerule translating a setting from a given parameter to another parameter
        
            Parameters:
                parameterRelation (cern.lsa.domain.settings.parameter.relation.ParameterRelation): non-null parameter defining the searched makerule
        
            Returns:
                the non-null :code:`MakeRuleConfigInfo` containing the configuration status (
                :code:`MakeRuleConfigInfo.MakeRuleConfigStatus`).
        
        
        """
        ...
    def findParameterByName(self, string: str) -> cern.lsa.domain.settings.Parameter:
        """
            Returns the parameter object identified by the given name or null if no parameter of that name can be found
        
            Parameters:
                parameterName (java.lang.String): the non null name of the parameter to retrieve
        
            Returns:
                the parameter identified by the name or null if the name does not match any existing parameter
        
        
        """
        ...
    def findParameterGroupsByAccelerator(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.ParameterGroup]:
        """
            Returns all parameter groups for the given accelerator
        
            Parameters:
                accelerator (cern.accsoft.commons.domain.Accelerator): to search the parameter groups for
        
            Returns:
                set of parameter groups or an empty set, if none were found
        
        
        """
        ...
    def findParameterRelationInfos(self, parameterRelationInfosRequest: cern.lsa.domain.settings.parameter.relation.ParameterRelationInfosRequest) -> java.util.Set[cern.lsa.domain.settings.parameter.relation.ParameterRelationInfo]:
        """
        
            Returns:
                information about parameter relations
        
        
        """
        ...
    def findParameterTrees(self, parameterTreesRequest: cern.lsa.domain.settings.ParameterTreesRequest) -> java.util.Set[cern.lsa.domain.settings.ParameterTreeNode]:
        """
            Builds the tree of parameters, starting at the given root parameters, in the requested direction. The tree direction can
            be:
        
              - :code:`ParameterTreesRequest.TreeDirection.DEPENDENT_TREE` means that the caller wants the tree of all parameters
                dependents
              - :code:`ParameterTreesRequest.TreeDirection.SOURCE_TREE` means that the caller wants the tree of all parameters sources
        
            *Attention:* this method does not verify the existence of the parameter names passed as an argument. It is the caller's
            responsibility to ensure that the parameter names are valid. If an unknown parameter name is passed, a
            :code:`ParameterTreeNode` object with no parent and no child is returned. In other words, this method cannot distinguish
            between a valid parameter without dependents and an invalid parameter.
        
            Returns:
                a recursive data structure that contains the tree of parameters
        
        
        """
        ...
    def findParameterTypeRelationInfos(self, parameterTypeRelationInfosRequest: cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelationInfosRequest) -> java.util.Set[cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelationInfo]:
        """
        
            Returns:
                information about parameter type relations
        
        
        """
        ...
    def findParameterTypes(self, parameterTypesRequest: cern.lsa.domain.settings.ParameterTypesRequest) -> java.util.Set[cern.lsa.domain.settings.ParameterType]:
        """
            Returns a set of the :code:`ParameterType`s for each of the given :code:`DeviceType name`
        
            The parameter types in the returned set are not sorted in any way.
        
            Parameters:
                request (cern.lsa.domain.settings.ParameterTypesRequest): container for parameter types criteria
        
            Returns:
                a non-null immutable set of parameters matching given criteria
        
        
        """
        ...
    def findParameters(self, parametersRequest: cern.lsa.domain.settings.ParametersRequest) -> java.util.Set[cern.lsa.domain.settings.Parameter]:
        """
            Returns parameters matching criteria specified in the given request.
        
            The parameters in the returned set are not sorted in any way.
        
            Parameters:
                request (cern.lsa.domain.settings.ParametersRequest): container for parameters criteria
        
            Returns:
                a non-null immutable set of parameters matching given criteria
        
        
        """
        ...
    def findParametersByDeviceProperty(self, string: str) -> java.util.Set[cern.lsa.domain.settings.Parameter]:
        """
            Returns all parameters belonging to device property or empty set.
        
        """
        ...
    def findParametersForEditing(self, parametersRequest: cern.lsa.domain.settings.ParametersRequest) -> java.util.Set[cern.lsa.domain.settings.ParameterForEditing]:
        """
            Returns parameters for editing matching criteria specified in the given request. Results are not sorted in any way.
        
            Parameters:
                request (cern.lsa.domain.settings.ParametersRequest): container for parameters criteria
        
            Returns:
                a non-null immutable set of parameters for editing matching given criteria
        
        
        """
        ...
    def findParametersWithSettings(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> java.util.Set[cern.lsa.domain.settings.Parameter]:
        """
            Returns a set of parameters :code:`Parameter` for which there is settings within given :code:`StandAloneContext`
        
            Parameters:
                context (cern.lsa.domain.settings.StandAloneContext): the context used to find the parameters with settings
        
            Returns:
                the non-null set of parameters
        
        
        """
        ...
    def findParametersWithoutSettings(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> java.util.Set[cern.lsa.domain.settings.Parameter]:
        """
            Returns a set of parameters :code:`Parameter` for which there is no settings within given :code:`StandAloneContext`
        
            Parameters:
                context (cern.lsa.domain.settings.StandAloneContext): the context used to find the parameters without settings
        
            Returns:
                the non-null set of parameters
        
        
        """
        ...
    def getMaxDelta(self, parameter: cern.lsa.domain.settings.Parameter) -> float:
        """
            Returns the maximum change (delta) that can be applied to the parameter's value when it is trimmed.
        
            Returns:
                the maximum delta of parameter or :code:`NaN` if it's not defined
        
        
        """
        ...
    def removeParametersFromParameterGroup(self, parameterGroup: cern.lsa.domain.settings.ParameterGroup, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> None: ...
    def saveCriticalProperty(self, propertyVersion: cern.lsa.domain.devices.type.PropertyVersion, device: cern.lsa.domain.devices.Device) -> None: ...
    def saveParameterGroup(self, parameterGroup: cern.lsa.domain.settings.ParameterGroup) -> None: ...
    def saveParameterRelationInfos(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.parameter.relation.ParameterRelationInfo], typing.Sequence[cern.lsa.domain.settings.parameter.relation.ParameterRelationInfo]]) -> None: ...
    def saveParameterTypeRelationInfos(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelationInfo], typing.Sequence[cern.lsa.domain.settings.parameter.type.relation.ParameterTypeRelationInfo]]) -> None: ...
    def saveParameterTypes(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.ParameterType], typing.Sequence[cern.lsa.domain.settings.ParameterType]]) -> None: ...
    def saveParameters(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.ParameterAttributes], typing.Sequence[cern.lsa.domain.settings.ParameterAttributes]]) -> None: ...

class CommonServiceLocator:
    """
    @Deprecated public class CommonServiceLocator extends java.lang.Object
    
        Deprecated.
        use :class:`~cern.lsa.client.common.LsaConfigurationConstants` instead
    """
    JDBC_PROPERTIES: typing.ClassVar[str] = ...
    """
    public static final java.lang.String JDBC_PROPERTIES
    
        Deprecated.
    
        Also see:
            :meth:`~constant`
    
    
    """
    DATABASE_PROPERTY_NAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DATABASE_PROPERTY_NAME
    
        Deprecated.
    
        Also see:
            :meth:`~constant`
    
    
    """
    SERVER_PROPERTIES: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SERVER_PROPERTIES
    
        Deprecated.
    
        Also see:
            :meth:`~constant`
    
    
    """
    SERVER_PROPERTY_NAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SERVER_PROPERTY_NAME
    
        Deprecated.
    
        Also see:
            :meth:`~constant`
    
    
    """
    MODE_PROPERTY_NAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String MODE_PROPERTY_NAME
    
        Deprecated.
    
        Also see:
            :meth:`~constant`
    
    
    """
    @staticmethod
    def isTwoTier() -> bool:
        """
            Deprecated.
        
        """
        ...

class CommonSettingService:
    """
    public interface CommonSettingService
    
        Service allowing to fetch current and historical settings for specified context.
    """
    def deleteSettings(self, settingsDeletionRequest: cern.lsa.domain.settings.SettingsDeletionRequest) -> None:
        """
            Deletes settings for the given set of parameters in the given context(s).
        
            Parameters:
                settingsDeletionRequest (cern.lsa.domain.settings.SettingsDeletionRequest): object containing the description of the request
        
        
        """
        ...
    def findContextSettings(self, contextSettingsRequest: cern.lsa.domain.settings.ContextSettingsRequest) -> cern.lsa.domain.settings.ContextSettings:
        """
            Finds all the context settings associated to all beam processes corresponding to the given
            :code:`ContextSettingsRequest`.
        
        
            See also :code:`SettingFinder#findContextSettings(ContextSettingsRequest)`
        
            Parameters:
                contextSettingsRequest (cern.lsa.domain.settings.ContextSettingsRequest): the non :code:`null` contextSettingsRequest
        
            Returns:
                a non :code:`null` :code:`ContextSettings` object containing settings of all parameters
        
        
        """
        ...
    def findNotIncorporatedParameters(self, standAloneBeamProcess: cern.lsa.domain.settings.StandAloneBeamProcess, int: int, beamProcess: cern.lsa.domain.settings.BeamProcess, int2: int) -> cern.lsa.domain.settings.NotIncorporatedParameters:
        """
            Finds and returns names of parameters that have different values in the source and destination beam process at specified
            points. For details see :code:`SettingFinder#findNotIncorporatedParameters(StandAloneBeamProcess, int,
            StandAloneBeamProcess, int)`.
        
        """
        ...

class CommonTimingService:
    """
    public interface CommonTimingService
    
        An object implementing this interface gives to the clients access to the timing services
    """
    ...

class CommonTrimService:
    """
    public interface CommonTrimService
    
        An object implementing this interface provides the implementation of the trim of a value.
    """
    def copySettings(self, copySettingsRequest: cern.lsa.domain.settings.CopySettingsRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    def deleteTrimById(self, long: int) -> None: ...
    def findParametersAndParameterGroupsByBPsSinceTime(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.BeamProcess], typing.Sequence[cern.lsa.domain.settings.BeamProcess]], date: java.util.Date) -> java.util.Map[str, java.util.Set[str]]:
        """
            Finds all parameters for the given beamprocesses that were touched by a trim since the given date.
        
            Parameters:
                beamProcesses (java.util.Collection<? extends cern.lsa.domain.settings.BeamProcess> beamProcesses): the beamprocesses to find the changed parameters for
                since (java.util.Date): the time since when the changed values should be searched
        
            Returns:
                the parameters that have changed settings in the given beamprocesses since the given time. They are grouped by parameter
                groups.
        
        
        """
        ...
    def findTrimHeaders(self, trimHeadersRequest: cern.lsa.domain.settings.TrimHeadersRequest) -> java.util.List[cern.lsa.domain.settings.TrimHeader]:
        """
            Finds all headers of trims performed after the given time in the contexts for the specified parameters. The trims are
            returned in chronological order starting from the oldest to the latest one. Any trim registered as affecting the given
            parameter and intersecting the given context is returned.
        
            Parameters:
                trimHeaderRequest (cern.lsa.domain.settings.TrimHeadersRequest): object containing the description of the request
        
            Returns:
                coresponding trim headers
        
        
        """
        ...
    def findTrimmedParameters(self, long: int) -> java.util.Set[cern.lsa.domain.settings.Parameter]:
        """
            Finds all parameters touched by a given trim. If no parameter was touched / given trim id did not exist, an empty set is
            returned.
        
            Parameters:
                trimId (long): Trim id
        
        
        """
        ...
    @typing.overload
    def incorporate(self, beamProcessIncorporationRequest: cern.lsa.domain.settings.BeamProcessIncorporationRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    @typing.overload
    def incorporate(self, incorporationRequest: cern.lsa.domain.settings.IncorporationRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    @typing.overload
    def revertTrim(self, revertTrimRequest: cern.lsa.domain.settings.RevertTrimRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    @typing.overload
    def revertTrim(self, list: java.util.List[cern.lsa.domain.settings.RevertTrimRequest]) -> java.util.List[cern.lsa.domain.settings.TrimResponse]: ...
    @typing.overload
    def trimSettings(self, trimRequest: cern.lsa.domain.settings.TrimRequest) -> cern.lsa.domain.settings.TrimResponse: ...
    @typing.overload
    def trimSettings(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.settings.TrimRequest], typing.Sequence[cern.lsa.domain.settings.TrimRequest]]) -> java.util.List[cern.lsa.domain.settings.TrimResponse]: ...
    def updateTrimDescription(self, long: int, string: str) -> None:
        """
            Update the description of a trim identified by the given :code:`trimId`. If no trim with that id can be found, no update
            is performed.
        
            Parameters:
                trimId (long): the id of the trim to update
                description (java.lang.String): the new description of the trim
        
        
        """
        ...

class LsaConfiguration:
    """
    public class LsaConfiguration extends java.lang.Object
    """
    @staticmethod
    def findDatabasePropertiesFileName() -> str: ...
    @typing.overload
    @staticmethod
    def findServerPropertiesFileName() -> str: ...
    @typing.overload
    @staticmethod
    def findServerPropertiesFileName(collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> str: ...

class LsaConfigurationConstants:
    """
    public class LsaConfigurationConstants extends java.lang.Object
    """
    SYSPROP_DATABASE_PROPERTIES_FILENAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SYSPROP_DATABASE_PROPERTIES_FILENAME
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    SYSPROP_DATABASE_NAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SYSPROP_DATABASE_NAME
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    SYSPROP_SERVER_PROPERTIES_FILENAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SYSPROP_SERVER_PROPERTIES_FILENAME
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    SYSPROP_SERVER_NAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SYSPROP_SERVER_NAME
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    SYSPROP_MODE_NAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SYSPROP_MODE_NAME
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    MODE_3_TIER: typing.ClassVar[str] = ...
    """
    public static final java.lang.String MODE_3_TIER
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    MODE_2_TIER: typing.ClassVar[str] = ...
    """
    public static final java.lang.String MODE_2_TIER
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    SYSPROP_SERVER_SPRING_CACHE_ENABLED: typing.ClassVar[str] = ...
    """
    public static final java.lang.String SYSPROP_SERVER_SPRING_CACHE_ENABLED
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class TransactionService:
    """
    public interface TransactionService
    
        Service that handles the generation of transaction ids used by the exploitation sub-system
    """
    def generateTransactionId(self) -> int:
        """
            Generates a transaction id that can be used by exploitation sub-system. This id can be set as an attribute in the
            :code:`TrimRequest` and :code:`DriveRequest`.
        
            Returns:
                the new transaction id
        
        
        """
        ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.common")``.

    ClientException: typing.Type[ClientException]
    CommonAcceleratorService: typing.Type[CommonAcceleratorService]
    CommonArchiveReferenceService: typing.Type[CommonArchiveReferenceService]
    CommonCacheService: typing.Type[CommonCacheService]
    CommonContextService: typing.Type[CommonContextService]
    CommonDeviceService: typing.Type[CommonDeviceService]
    CommonExploitationService: typing.Type[CommonExploitationService]
    CommonGenerationService: typing.Type[CommonGenerationService]
    CommonJapcService: typing.Type[CommonJapcService]
    CommonKnobService: typing.Type[CommonKnobService]
    CommonOpticService: typing.Type[CommonOpticService]
    CommonParameterService: typing.Type[CommonParameterService]
    CommonServiceLocator: typing.Type[CommonServiceLocator]
    CommonSettingService: typing.Type[CommonSettingService]
    CommonTimingService: typing.Type[CommonTimingService]
    CommonTrimService: typing.Type[CommonTrimService]
    LsaConfiguration: typing.Type[LsaConfiguration]
    LsaConfigurationConstants: typing.Type[LsaConfigurationConstants]
    TransactionService: typing.Type[TransactionService]
    cache: cern.lsa.client.common.cache.__module_protocol__
    japc: cern.lsa.client.common.japc.__module_protocol__
    spi: cern.lsa.client.common.spi.__module_protocol__
    test: cern.lsa.client.common.test.__module_protocol__
