import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.beamdestinations
import cern.accsoft.commons.domain.modes
import cern.lsa.client
import cern.lsa.client.rest.api.v1
import cern.lsa.client.rest.api.v1.feign
import cern.lsa.client.rest.cern.api.v1.feign
import cern.lsa.domain.cern.timing
import cern.lsa.domain.devices
import cern.lsa.domain.devices.inca
import cern.lsa.domain.optics
import cern.lsa.domain.settings
import com.fasterxml.jackson.databind
import java.util
import typing



class CernClientRestConfig(cern.lsa.client.rest.api.v1.AbstractClientRestConfig):
    """
    @Configuration @PropertySource(value="classpath:${lsa.server.properties}", ignoreResourceNotFound=true) @Import(:class:`~cern.lsa.client.rest.cern.api.v1.CernObjectMapperConfig`) public class CernClientRestConfig extends cern.lsa.client.rest.api.v1.AbstractClientRestConfig
    """
    def __init__(self): ...

class CernObjectMapperConfig(cern.lsa.client.rest.api.v1.AbstractObjectMapperConfig):
    """
    @Configuration public class CernObjectMapperConfig extends cern.lsa.client.rest.api.v1.AbstractObjectMapperConfig
    """
    def __init__(self): ...
    def objectMapper(self) -> com.fasterxml.jackson.databind.ObjectMapper: ...

class ClientRestAcceleratorService(cern.lsa.client.rest.api.v1.ClientRestCommonAcceleratorService, cern.lsa.client.AcceleratorService):
    """
    public class ClientRestAcceleratorService extends cern.lsa.client.rest.api.v1.ClientRestCommonAcceleratorService implements cern.lsa.client.AcceleratorService
    """
    def __init__(self, acceleratorFeignService: cern.lsa.client.rest.cern.api.v1.feign.AcceleratorFeignService): ...
    def findActiveAcceleratorMode(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> cern.accsoft.commons.domain.modes.AcceleratorMode:
        """
        
            Specified by:
                :code:`findActiveAcceleratorMode` in interface :code:`cern.lsa.client.AcceleratorService`
        
        
        """
        ...
    def findActiveBeamDestinationEndPoint(self, beamDestination: cern.accsoft.commons.domain.beamdestinations.BeamDestination) -> cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint:
        """
        
            Specified by:
                :code:`findActiveBeamDestinationEndPoint` in interface :code:`cern.lsa.client.AcceleratorService`
        
        
        """
        ...
    def findOpConfigs(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[str]:
        """
        
            Specified by:
                :code:`findOpConfigs` in interface :code:`cern.lsa.client.AcceleratorService`
        
        
        """
        ...
    def setActiveAcceleratorMode(self, accelerator: cern.accsoft.commons.domain.Accelerator, acceleratorMode: cern.accsoft.commons.domain.modes.AcceleratorMode) -> None:
        """
        
            Specified by:
                :code:`setActiveAcceleratorMode` in interface :code:`cern.lsa.client.AcceleratorService`
        
        
        """
        ...
    def setActiveBeamDestinationEndPoint(self, beamDestination: cern.accsoft.commons.domain.beamdestinations.BeamDestination, beamDestinationEndPoint: cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint) -> None:
        """
        
            Specified by:
                :code:`setActiveBeamDestinationEndPoint` in interface :code:`cern.lsa.client.AcceleratorService`
        
        
        """
        ...

class ClientRestContextService(cern.lsa.client.rest.api.v1.ClientRestCommonContextService, cern.lsa.client.ContextService):
    """
    public class ClientRestContextService extends cern.lsa.client.rest.api.v1.ClientRestCommonContextService implements cern.lsa.client.ContextService
    """
    def __init__(self, commonAcceleratorUserFeignService: cern.lsa.client.rest.api.v1.feign.CommonAcceleratorUserFeignService, commonBeamProcessPurposeFeignService: cern.lsa.client.rest.api.v1.feign.CommonBeamProcessPurposeFeignService, commonContextCategoryFeignService: cern.lsa.client.rest.api.v1.feign.CommonContextCategoryFeignService, commonContextFeignService: cern.lsa.client.rest.api.v1.feign.CommonContextFeignService, commonDrivableContextFeignService: cern.lsa.client.rest.api.v1.feign.CommonDrivableContextFeignService, commonStandAloneBeamProcessFeignService: cern.lsa.client.rest.api.v1.feign.CommonStandAloneBeamProcessFeignService, standAloneCycleFeignService: cern.lsa.client.rest.cern.api.v1.feign.StandAloneCycleFeignService, standAloneContextFeignService: cern.lsa.client.rest.cern.api.v1.feign.StandAloneContextFeignService, timingUserFeignService: cern.lsa.client.rest.cern.api.v1.feign.TimingUserFeignService): ...
    def findActiveContexts(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.StandAloneContext]:
        """
        
            Specified by:
                :code:`findActiveContexts` in interface :code:`cern.lsa.client.ContextService`
        
        
        """
        ...
    def findActiveTimingUsers(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> cern.lsa.domain.cern.timing.ActiveTimingUsers:
        """
        
            Specified by:
                :code:`findActiveTimingUsers` in interface :code:`cern.lsa.client.ContextService`
        
        
        """
        ...
    def findLoggingHistory(self, long: int, long2: int, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.List[cern.lsa.domain.settings.UserContextMapping]:
        """
        
            Specified by:
                :code:`findLoggingHistory` in interface :code:`cern.lsa.client.ContextService`
        
        
        """
        ...
    def findStandAloneCycle(self, string: str) -> cern.lsa.domain.settings.StandAloneCycle:
        """
        
            Specified by:
                :code:`findStandAloneCycle` in interface :code:`cern.lsa.client.common.CommonContextService`
        
            Overrides:
                :code:`findStandAloneCycle` in class :code:`cern.lsa.client.rest.api.v1.ClientRestCommonContextService`
        
        
        """
        ...
    @typing.overload
    def findStandAloneCycles(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[cern.lsa.domain.settings.StandAloneCycle]:
        """
        
            Specified by:
                :code:`findStandAloneCycles` in interface :code:`cern.lsa.client.common.CommonContextService`
        
            Overrides:
                :code:`findStandAloneCycles` in class :code:`cern.lsa.client.rest.api.v1.ClientRestCommonContextService`
        
        
            Specified by:
                :code:`findStandAloneCycles` in interface :code:`cern.lsa.client.common.CommonContextService`
        
            Overrides:
                :code:`findStandAloneCycles` in class :code:`cern.lsa.client.rest.api.v1.ClientRestCommonContextService`
        
        
        """
        ...
    @typing.overload
    def findStandAloneCycles(self, standAloneCyclesRequest: cern.lsa.domain.settings.StandAloneCyclesRequest) -> java.util.Set[cern.lsa.domain.settings.StandAloneCycle]: ...

class ClientRestDeviceService(cern.lsa.client.rest.api.v1.ClientRestCommonDeviceService, cern.lsa.client.DeviceService):
    """
    public class ClientRestDeviceService extends cern.lsa.client.rest.api.v1.ClientRestCommonDeviceService implements cern.lsa.client.DeviceService
    """
    def __init__(self, deviceFeignService: cern.lsa.client.rest.cern.api.v1.feign.DeviceFeignService, commonCalibrationFeignService: cern.lsa.client.rest.api.v1.feign.CommonCalibrationFeignService, commonDeviceGroupFeignService: cern.lsa.client.rest.api.v1.feign.CommonDeviceGroupFeignService, commonDeviceGroupTypeFeignService: cern.lsa.client.rest.api.v1.feign.CommonDeviceGroupTypeFeignService, commonDeviceTypeFeignService: cern.lsa.client.rest.api.v1.feign.CommonDeviceTypeFeignService, commonLogicalDeviceFeignService: cern.lsa.client.rest.api.v1.feign.CommonLogicalDeviceFeignService): ...
    def findPowerConverterInfosByDeviceIds(self, collection: typing.Union[java.util.Collection[int], typing.Sequence[int]]) -> java.util.Set[cern.lsa.domain.optics.PowerConverterInfo]:
        """
        
            Specified by:
                :code:`findPowerConverterInfosByDeviceIds` in interface :code:`cern.lsa.client.DeviceService`
        
        
        """
        ...
    def findPowerConverterNestedCircuitInfos(self, powerConverterNestedCircuitInfosRequest: cern.lsa.domain.devices.PowerConverterNestedCircuitInfosRequest) -> java.util.Set[cern.lsa.domain.devices.PowerConverterNestedCircuitInfo]:
        """
        
            Specified by:
                :code:`findPowerConverterNestedCircuitInfos` in interface :code:`cern.lsa.client.DeviceService`
        
        
        """
        ...

class ClientRestHyperCycleService(cern.lsa.client.HyperCycleService):
    """
    public class ClientRestHyperCycleService extends java.lang.Object implements cern.lsa.client.HyperCycleService
    """
    def __init__(self, hyperCycleFeignService: cern.lsa.client.rest.cern.api.v1.feign.HyperCycleFeignService): ...
    def findActiveHyperCycle(self) -> cern.lsa.domain.settings.HyperCycle:
        """
        
            Specified by:
                :code:`findActiveHyperCycle` in interface :code:`cern.lsa.client.HyperCycleService`
        
        
        """
        ...
    def findHyperCycle(self, string: str) -> cern.lsa.domain.settings.HyperCycle:
        """
        
            Specified by:
                :code:`findHyperCycle` in interface :code:`cern.lsa.client.HyperCycleService`
        
        
        """
        ...
    def findHyperCycles(self) -> java.util.Set[cern.lsa.domain.settings.HyperCycle]:
        """
        
            Specified by:
                :code:`findHyperCycles` in interface :code:`cern.lsa.client.HyperCycleService`
        
        
        """
        ...
    def makeHyperCycleActive(self, string: str) -> None:
        """
        
            Specified by:
                :code:`makeHyperCycleActive` in interface :code:`cern.lsa.client.HyperCycleService`
        
        
        """
        ...
    def removeHyperCycle(self, hyperCycle: cern.lsa.domain.settings.HyperCycle) -> None:
        """
        
            Specified by:
                :code:`removeHyperCycle` in interface :code:`cern.lsa.client.HyperCycleService`
        
        
        """
        ...
    def saveHyperCycle(self, hyperCycle: cern.lsa.domain.settings.HyperCycle) -> None:
        """
        
            Specified by:
                :code:`saveHyperCycle` in interface :code:`cern.lsa.client.HyperCycleService`
        
        
        """
        ...

class ClientRestIncaService(cern.lsa.client.IncaService):
    """
    public class ClientRestIncaService extends java.lang.Object implements cern.lsa.client.IncaService
    """
    def __init__(self, deviceTypeFeignService: cern.lsa.client.rest.cern.api.v1.feign.DeviceTypeFeignService): ...
    def findIncaPropertyFieldInfos(self, incaPropertyFieldInfosRequest: cern.lsa.domain.devices.inca.IncaPropertyFieldInfosRequest) -> java.util.Set[cern.lsa.domain.devices.inca.IncaPropertyFieldInfo]:
        """
        
            Specified by:
                :code:`findIncaPropertyFieldInfos` in interface :code:`cern.lsa.client.IncaService`
        
        
        """
        ...
    def saveIncaPropertyFieldInfos(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.devices.inca.IncaPropertyFieldInfo], typing.Sequence[cern.lsa.domain.devices.inca.IncaPropertyFieldInfo]]) -> None:
        """
        
            Specified by:
                :code:`saveIncaPropertyFieldInfos` in interface :code:`cern.lsa.client.IncaService`
        
        
        """
        ...

class ClientRestOpticService(cern.lsa.client.rest.api.v1.ClientRestCommonOpticService, cern.lsa.client.OpticService):
    """
    public class ClientRestOpticService extends cern.lsa.client.rest.api.v1.ClientRestCommonOpticService implements cern.lsa.client.OpticService
    """
    def __init__(self, commonElementFeignService: cern.lsa.client.rest.api.v1.feign.CommonElementFeignService, commonOpticsTableFeignService: cern.lsa.client.rest.api.v1.feign.CommonOpticsTableFeignService, commonMeasuredTwissFeignService: cern.lsa.client.rest.api.v1.feign.CommonMeasuredTwissFeignService, commonBeamProcessTypeFeignService: cern.lsa.client.rest.api.v1.feign.CommonBeamProcessTypeFeignService, opticFeignService: cern.lsa.client.rest.cern.api.v1.feign.OpticFeignService, twissFeignService: cern.lsa.client.rest.cern.api.v1.feign.TwissFeignService, standAloneBeamProcessFeignService: cern.lsa.client.rest.cern.api.v1.feign.StandAloneBeamProcessFeignService, particleTransferFeignService: cern.lsa.client.rest.cern.api.v1.feign.ParticleTransferFeignService): ...
    def deleteOptic(self, optic: cern.lsa.domain.optics.Optic) -> None:
        """
        
            Specified by:
                :code:`deleteOptic` in interface :code:`cern.lsa.client.OpticService`
        
        
        """
        ...
    def findConfigurationParameters(self, string: str) -> java.util.Map[str, str]:
        """
        
            Specified by:
                :code:`findConfigurationParameters` in interface :code:`cern.lsa.client.OpticService`
        
        
        """
        ...
    def findContextOpticsTables(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> java.util.Collection[cern.lsa.domain.optics.OpticsTable]:
        """
        
            Specified by:
                :code:`findContextOpticsTables` in interface :code:`cern.lsa.client.OpticService`
        
        
        """
        ...
    def saveOptic(self, optic: cern.lsa.domain.optics.Optic) -> None:
        """
        
            Specified by:
                :code:`saveOptic` in interface :code:`cern.lsa.client.OpticService`
        
        
        """
        ...
    def saveTwisses(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.optics.Twiss], typing.Sequence[cern.lsa.domain.optics.Twiss]]) -> None:
        """
        
            Specified by:
                :code:`saveTwisses` in interface :code:`cern.lsa.client.OpticService`
        
        
        """
        ...
    def updateOptic(self, optic: cern.lsa.domain.optics.Optic) -> None:
        """
        
            Specified by:
                :code:`updateOptic` in interface :code:`cern.lsa.client.OpticService`
        
        
        """
        ...
    def updateOpticForActualBeamProcess(self, standAloneBeamProcess: cern.lsa.domain.settings.StandAloneBeamProcess, string: str) -> None:
        """
        
            Specified by:
                :code:`updateOpticForActualBeamProcess` in interface :code:`cern.lsa.client.OpticService`
        
        
        """
        ...
    def updateTwisses(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.optics.Twiss], typing.Sequence[cern.lsa.domain.optics.Twiss]]) -> None:
        """
        
            Specified by:
                :code:`updateTwisses` in interface :code:`cern.lsa.client.OpticService`
        
        
        """
        ...

class ClientRestParameterService(cern.lsa.client.rest.api.v1.ClientRestCommonParameterService, cern.lsa.client.ParameterService):
    """
    public class ClientRestParameterService extends cern.lsa.client.rest.api.v1.ClientRestCommonParameterService implements cern.lsa.client.ParameterService
    """
    def __init__(self, commonParameterTypeFeignService: cern.lsa.client.rest.api.v1.feign.CommonParameterTypeFeignService, commonMakeRuleFeignService: cern.lsa.client.rest.api.v1.feign.CommonMakeRuleFeignService, commonParameterGroupFeignService: cern.lsa.client.rest.api.v1.feign.CommonParameterGroupFeignService, commonParameterHierarchyFeignService: cern.lsa.client.rest.api.v1.feign.CommonParameterHierarchyFeignService, commonParameterRelationFeignService: cern.lsa.client.rest.api.v1.feign.CommonParameterRelationFeignService, commonParameterTypeRelationFeignService: cern.lsa.client.rest.api.v1.feign.CommonParameterTypeRelationFeignService, parameterFeignService: cern.lsa.client.rest.cern.api.v1.feign.ParameterFeignService, deviceFeignService: cern.lsa.client.rest.cern.api.v1.feign.DeviceFeignService): ...
    def findParametersInKnobs(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[str]:
        """
        
            Specified by:
                :code:`findParametersInKnobs` in interface :code:`cern.lsa.client.ParameterService`
        
        
        """
        ...
    def findParametersInWorkingSets(self, accelerator: cern.accsoft.commons.domain.Accelerator) -> java.util.Set[str]:
        """
        
            Specified by:
                :code:`findParametersInWorkingSets` in interface :code:`cern.lsa.client.ParameterService`
        
        
        """
        ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.rest.cern.api.v1")``.

    CernClientRestConfig: typing.Type[CernClientRestConfig]
    CernObjectMapperConfig: typing.Type[CernObjectMapperConfig]
    ClientRestAcceleratorService: typing.Type[ClientRestAcceleratorService]
    ClientRestContextService: typing.Type[ClientRestContextService]
    ClientRestDeviceService: typing.Type[ClientRestDeviceService]
    ClientRestHyperCycleService: typing.Type[ClientRestHyperCycleService]
    ClientRestIncaService: typing.Type[ClientRestIncaService]
    ClientRestOpticService: typing.Type[ClientRestOpticService]
    ClientRestParameterService: typing.Type[ClientRestParameterService]
    feign: cern.lsa.client.rest.cern.api.v1.feign.__module_protocol__
