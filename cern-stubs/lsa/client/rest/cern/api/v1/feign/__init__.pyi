import cern.accsoft.commons.domain.beamdestinations
import cern.accsoft.commons.domain.modes
import cern.lsa.client.rest.api.v1.feign
import cern.lsa.domain.cern.settings
import cern.lsa.domain.cern.timing
import cern.lsa.domain.devices
import cern.lsa.domain.devices.inca
import cern.lsa.domain.optics
import cern.lsa.domain.settings
import java.util
import typing



class AcceleratorFeignService(cern.lsa.client.rest.api.v1.feign.CommonAcceleratorFeignService):
    """
    public interface AcceleratorFeignService extends cern.lsa.client.rest.api.v1.feign.CommonAcceleratorFeignService
    """
    def getActiveAcceleratorMode(self, string: str) -> cern.accsoft.commons.domain.modes.AcceleratorMode: ...
    def getActiveBeamDestinationEndPoint(self, string: str, string2: str) -> cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint: ...
    def getOpConfigs(self, string: str) -> java.util.Set[str]: ...
    def setActiveAcceleratorMode(self, string: str, acceleratorMode: cern.accsoft.commons.domain.modes.AcceleratorMode) -> None: ...
    def setActiveBeamDestinationEndPoint(self, string: str, string2: str, beamDestinationEndPoint: cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint) -> None: ...

class AcceleratorResourceUrls:
    """
    public final class AcceleratorResourceUrls extends java.lang.Object
    """
    BEAM_DESTINATION_NAME_PARAM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BEAM_DESTINATION_NAME_PARAM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BEAM_DESTINATION_NAME_PATH_VARIABLE_CAPTOR: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BEAM_DESTINATION_NAME_PATH_VARIABLE_CAPTOR
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ENDPOINT_NAME_PARAM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ENDPOINT_NAME_PARAM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ENDPOINT_NAME_PATH_VARIABLE_CAPTOR: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ENDPOINT_NAME_PATH_VARIABLE_CAPTOR
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    OP_CONFIGS_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String OP_CONFIGS_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ACTIVE_MODE_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ACTIVE_MODE_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BEAM_DESTINATIONS_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BEAM_DESTINATIONS_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BEAM_DESTINATION_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BEAM_DESTINATION_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BEAM_DESTINATION_ENDPOINTS_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BEAM_DESTINATION_ENDPOINTS_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BEAM_DESTINATION_ENDPOINT_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BEAM_DESTINATION_ENDPOINT_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ACTIVE_ENDPOINT_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ACTIVE_ENDPOINT_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """

class DeviceFeignService(cern.lsa.client.rest.api.v1.feign.CommonDeviceFeignService):
    """
    public interface DeviceFeignService extends cern.lsa.client.rest.api.v1.feign.CommonDeviceFeignService
    """
    def findPowerConverterInfosByDeviceIds(self, collection: typing.Union[java.util.Collection[int], typing.Sequence[int]]) -> java.util.Set[cern.lsa.domain.optics.PowerConverterInfo]: ...
    def findPowerConverterNestedCircuitInfos(self, powerConverterNestedCircuitInfosRequest: cern.lsa.domain.devices.PowerConverterNestedCircuitInfosRequest) -> java.util.Set[cern.lsa.domain.devices.PowerConverterNestedCircuitInfo]: ...

class DeviceResourceUrls:
    """
    public class DeviceResourceUrls extends java.lang.Object
    """
    DEVICE_SEARCH_POWER_CONVERTER_INFO_BY_DEVICE_IDS_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DEVICE_SEARCH_POWER_CONVERTER_INFO_BY_DEVICE_IDS_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DEVICE_SEARCH_POWER_CONVERTER_NESTED_CIRCUIT_INFO_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DEVICE_SEARCH_POWER_CONVERTER_NESTED_CIRCUIT_INFO_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class DeviceTypeFeignService(cern.lsa.client.rest.api.v1.feign.CommonDeviceTypeFeignService):
    """
    public interface DeviceTypeFeignService extends cern.lsa.client.rest.api.v1.feign.CommonDeviceTypeFeignService
    """
    def findIncaPropertyFieldInfos(self, incaPropertyFieldInfosRequest: cern.lsa.domain.devices.inca.IncaPropertyFieldInfosRequest) -> java.util.Set[cern.lsa.domain.devices.inca.IncaPropertyFieldInfo]: ...
    def saveIncaPropertyFieldInfos(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.devices.inca.IncaPropertyFieldInfo], typing.Sequence[cern.lsa.domain.devices.inca.IncaPropertyFieldInfo]]) -> None: ...

class DeviceTypeResourceUrls:
    """
    public class DeviceTypeResourceUrls extends java.lang.Object
    """
    DEVICE_TYPES_SEARCH_INCA_PROPERTY_FIELD_INFOS_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DEVICE_TYPES_SEARCH_INCA_PROPERTY_FIELD_INFOS_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DEVICE_TYPES_SAVE_INCA_PROPERTY_FIELD_INFOS_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DEVICE_TYPES_SAVE_INCA_PROPERTY_FIELD_INFOS_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class HyperCycleFeignService(cern.lsa.client.rest.api.v1.feign.FeignService):
    """
    public interface HyperCycleFeignService extends cern.lsa.client.rest.api.v1.feign.FeignService
    """
    def getActiveHyperCycle(self) -> cern.lsa.domain.settings.HyperCycle: ...
    def getAllHyperCycles(self) -> java.util.Set[cern.lsa.domain.settings.HyperCycle]: ...
    def getHyperCycle(self, string: str) -> cern.lsa.domain.settings.HyperCycle: ...
    def makeHyperCycleActive(self, string: str) -> None: ...
    def removeHyperCycle(self, string: str) -> None: ...
    def saveHyperCycle(self, hyperCycle: cern.lsa.domain.settings.HyperCycle) -> None: ...

class HyperCycleResourceUrls:
    """
    public final class HyperCycleResourceUrls extends java.lang.Object
    """
    HYPER_CYCLES_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String HYPER_CYCLES_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    HYPER_CYCLE_NAME_PARAM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String HYPER_CYCLE_NAME_PARAM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    HYPER_CYCLE_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String HYPER_CYCLE_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    HYPER_CYCLE_BY_NAME_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String HYPER_CYCLE_BY_NAME_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    HYPER_CYCLE_GET_ACTIVE_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String HYPER_CYCLE_GET_ACTIVE_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    HYPER_CYCLE_MAKE_ACTIVE_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String HYPER_CYCLE_MAKE_ACTIVE_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class OpticFeignService(cern.lsa.client.rest.api.v1.feign.CommonOpticFeignService):
    """
    public interface OpticFeignService extends cern.lsa.client.rest.api.v1.feign.CommonOpticFeignService
    """
    def deleteOptic(self, string: str) -> None: ...
    def saveOptic(self, optic: cern.lsa.domain.optics.Optic) -> None: ...
    def updateOptic(self, optic: cern.lsa.domain.optics.Optic) -> None: ...

class ParameterFeignService(cern.lsa.client.rest.api.v1.feign.CommonParameterFeignService):
    """
    public interface ParameterFeignService extends cern.lsa.client.rest.api.v1.feign.CommonParameterFeignService
    """
    def findParametersInKnobs(self, string: str) -> java.util.Set[str]: ...
    def findParametersInWorkingSets(self, string: str) -> java.util.Set[str]: ...

class ParameterResourceUrls:
    """
    public class ParameterResourceUrls extends java.lang.Object
    """
    PARAMETERS_SEARCH_IN_WORKING_SETS_BY_ACCELERATOR_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String PARAMETERS_SEARCH_IN_WORKING_SETS_BY_ACCELERATOR_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    PARAMETERS_SEARCH_IN_WORKING_SETS_BY_ACCELERATOR_PARAMETERIZED_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String PARAMETERS_SEARCH_IN_WORKING_SETS_BY_ACCELERATOR_PARAMETERIZED_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    PARAMETERS_SEARCH_IN_KNOBS_BY_ACCELERATOR_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String PARAMETERS_SEARCH_IN_KNOBS_BY_ACCELERATOR_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    PARAMETERS_SEARCH_IN_KNOBS_BY_ACCELERATOR_PARAMETERIZED_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String PARAMETERS_SEARCH_IN_KNOBS_BY_ACCELERATOR_PARAMETERIZED_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class ParticleTransferFeignService(cern.lsa.client.rest.api.v1.feign.FeignService):
    """
    public interface ParticleTransferFeignService extends cern.lsa.client.rest.api.v1.feign.FeignService
    """
    def findConfigurationParameters(self, string: str) -> java.util.Map[str, str]: ...

class ParticleTransferResourceUrls:
    """
    public class ParticleTransferResourceUrls extends java.lang.Object
    """
    PARTICLE_TRANSFER_NAME_PARAM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String PARTICLE_TRANSFER_NAME_PARAM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    PARTICLE_TRANSFER_NAME_PATH_VARIABLE_CAPTOR: typing.ClassVar[str] = ...
    """
    public static final java.lang.String PARTICLE_TRANSFER_NAME_PATH_VARIABLE_CAPTOR
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    PARTICLE_TRANSFERS_STEERING_PARAMETERS_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String PARTICLE_TRANSFERS_STEERING_PARAMETERS_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class ReDriveSettingsFeignService(cern.lsa.client.rest.api.v1.feign.FeignService):
    """
    public interface ReDriveSettingsFeignService extends cern.lsa.client.rest.api.v1.feign.FeignService
    """
    def reDriveDeviceSettings(self, reDriveRequest: cern.lsa.domain.cern.settings.ReDriveRequest) -> cern.lsa.domain.cern.settings.ReDriveResponse: ...
    class Urls:
        REDRIVE_DEVICES: typing.ClassVar[str] = ...

class StandAloneBeamProcessFeignService(cern.lsa.client.rest.api.v1.feign.CommonStandAloneBeamProcessFeignService):
    """
    public interface StandAloneBeamProcessFeignService extends cern.lsa.client.rest.api.v1.feign.CommonStandAloneBeamProcessFeignService
    """
    def changeOpticForActualBeamProcess(self, string: str, string2: str) -> None: ...

class StandAloneBeamProcessResourceUrls:
    """
    public class StandAloneBeamProcessResourceUrls extends java.lang.Object
    """
    STAND_ALONE_BEAM_PROCESS_CHANGE_OPTIC_NAME_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String STAND_ALONE_BEAM_PROCESS_CHANGE_OPTIC_NAME_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class StandAloneContextFeignService(cern.lsa.client.rest.api.v1.feign.CommonStandAloneContextFeignService):
    """
    public interface StandAloneContextFeignService extends cern.lsa.client.rest.api.v1.feign.CommonStandAloneContextFeignService
    """
    def findLoggingHistory(self, loggedUserContextMappingHistoryRequest: cern.lsa.domain.cern.settings.LoggedUserContextMappingHistoryRequest) -> java.util.List[cern.lsa.domain.settings.UserContextMapping]: ...
    def getActiveContexts(self, string: str) -> java.util.Set[cern.lsa.domain.settings.StandAloneContext]: ...

class StandAloneContextResourceUrls:
    """
    public final class StandAloneContextResourceUrls extends java.lang.Object
    """
    START_TIME_MILLIS_PARAM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String START_TIME_MILLIS_PARAM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    END_TIME_MILLIS_PARAM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String END_TIME_MILLIS_PARAM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    STANDALONE_CONTEXTS_ACTIVE_CONTEXTS_BY_ACCELERATOR_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String STANDALONE_CONTEXTS_ACTIVE_CONTEXTS_BY_ACCELERATOR_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    STANDALONE_CONTEXTS_LOGGED_USER_CONTEXT_MAPPING_HISTORY_SEARCH_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String STANDALONE_CONTEXTS_LOGGED_USER_CONTEXT_MAPPING_HISTORY_SEARCH_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class StandAloneCycleFeignService(cern.lsa.client.rest.api.v1.feign.FeignService):
    """
    public interface StandAloneCycleFeignService extends cern.lsa.client.rest.api.v1.feign.FeignService
    """
    def findStandAloneCycles(self, standAloneCyclesRequest: cern.lsa.domain.settings.StandAloneCyclesRequest) -> java.util.Set[cern.lsa.domain.settings.StandAloneCycle]: ...
    def getStandAloneCycle(self, string: str) -> cern.lsa.domain.settings.StandAloneCycle: ...

class StandAloneCycleResourceUrls:
    """
    public final class StandAloneCycleResourceUrls extends java.lang.Object
    """
    STANDALONE_CYCLE_NAME_PARAM: typing.ClassVar[str] = ...
    """
    public static final java.lang.String STANDALONE_CYCLE_NAME_PARAM
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    STANDALONE_CYCLE_NAME_PATH_VARIABLE_CAPTOR: typing.ClassVar[str] = ...
    """
    public static final java.lang.String STANDALONE_CYCLE_NAME_PATH_VARIABLE_CAPTOR
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    STANDALONE_CYCLES_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String STANDALONE_CYCLES_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    STANDALONE_CYCLE_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String STANDALONE_CYCLE_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    STANDALONE_CYCLES_BY_NAME_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String STANDALONE_CYCLES_BY_NAME_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    STANDALONE_CYCLES_SEARCH_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String STANDALONE_CYCLES_SEARCH_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class TimingUserFeignService(cern.lsa.client.rest.api.v1.feign.FeignService):
    """
    public interface TimingUserFeignService extends cern.lsa.client.rest.api.v1.feign.FeignService
    """
    def getActiveTimingUsers(self, string: str) -> cern.lsa.domain.cern.timing.ActiveTimingUsers: ...

class TimingUserResourceUrls:
    """
    public final class TimingUserResourceUrls extends java.lang.Object
    """
    TIMING_USERS_ACTIVE_BY_ACCELERATOR_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String TIMING_USERS_ACTIVE_BY_ACCELERATOR_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...

class TwissFeignService(cern.lsa.client.rest.api.v1.feign.CommonTwissFeignService):
    """
    public interface TwissFeignService extends cern.lsa.client.rest.api.v1.feign.CommonTwissFeignService
    """
    def saveTwisses(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.optics.Twiss], typing.Sequence[cern.lsa.domain.optics.Twiss]]) -> None: ...
    def updateTwisses(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.optics.Twiss], typing.Sequence[cern.lsa.domain.optics.Twiss]]) -> None: ...

class TwissResourceUrls:
    """
    public class TwissResourceUrls extends java.lang.Object
    """
    TWISSES_URI: typing.ClassVar[str] = ...
    """
    public static final java.lang.String TWISSES_URI
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.rest.cern.api.v1.feign")``.

    AcceleratorFeignService: typing.Type[AcceleratorFeignService]
    AcceleratorResourceUrls: typing.Type[AcceleratorResourceUrls]
    DeviceFeignService: typing.Type[DeviceFeignService]
    DeviceResourceUrls: typing.Type[DeviceResourceUrls]
    DeviceTypeFeignService: typing.Type[DeviceTypeFeignService]
    DeviceTypeResourceUrls: typing.Type[DeviceTypeResourceUrls]
    HyperCycleFeignService: typing.Type[HyperCycleFeignService]
    HyperCycleResourceUrls: typing.Type[HyperCycleResourceUrls]
    OpticFeignService: typing.Type[OpticFeignService]
    ParameterFeignService: typing.Type[ParameterFeignService]
    ParameterResourceUrls: typing.Type[ParameterResourceUrls]
    ParticleTransferFeignService: typing.Type[ParticleTransferFeignService]
    ParticleTransferResourceUrls: typing.Type[ParticleTransferResourceUrls]
    ReDriveSettingsFeignService: typing.Type[ReDriveSettingsFeignService]
    StandAloneBeamProcessFeignService: typing.Type[StandAloneBeamProcessFeignService]
    StandAloneBeamProcessResourceUrls: typing.Type[StandAloneBeamProcessResourceUrls]
    StandAloneContextFeignService: typing.Type[StandAloneContextFeignService]
    StandAloneContextResourceUrls: typing.Type[StandAloneContextResourceUrls]
    StandAloneCycleFeignService: typing.Type[StandAloneCycleFeignService]
    StandAloneCycleResourceUrls: typing.Type[StandAloneCycleResourceUrls]
    TimingUserFeignService: typing.Type[TimingUserFeignService]
    TimingUserResourceUrls: typing.Type[TimingUserResourceUrls]
    TwissFeignService: typing.Type[TwissFeignService]
    TwissResourceUrls: typing.Type[TwissResourceUrls]
