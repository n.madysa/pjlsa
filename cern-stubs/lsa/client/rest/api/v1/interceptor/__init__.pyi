import feign
import typing



class RbacInterceptor(feign.RequestInterceptor):
    def __init__(self): ...
    def apply(self, requestTemplate: feign.RequestTemplate) -> None: ...

class TracingInterceptor(feign.RequestInterceptor):
    def __init__(self): ...
    def apply(self, requestTemplate: feign.RequestTemplate) -> None: ...


class __module_protocol__(typing.Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.rest.api.v1.interceptor")``.

    RbacInterceptor: typing.Type[RbacInterceptor]
    TracingInterceptor: typing.Type[TracingInterceptor]
